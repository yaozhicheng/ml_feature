# coding=utf-8
# Copyright (c) 2016 Tinydot. inc.
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import tensorflow as tf
import os

def load_pbfile(url, flag=False):
    graph = tf.Graph()
    with graph.as_default():
        with open(url, 'rb') as f:
            graph_def = tf.GraphDef()
            graph_def.ParseFromString(f.read())
            tf.import_graph_def(graph_def, name='')
    if flag:
        return graph_def
    else:
        return graph

def test_op_config():
    '''
    测试配置文件中定义的Op与4个测试文件中一共差距多少
    :return:
    '''
    url = os.path.abspath(os.path.join(os.path.dirname(__file__), '..','tensorflow_model_test','alexnet_v2.pb'))
    graph = load_pbfile(url)
    s = set()
    for op in graph.get_operations():
        s.add(op.type)
    ban_list = ['Split','Less','Sigmoid','Assign','Tanh','Add','Range','Transpose','StridedSlice','ExpandDims','Fill',\
                'Placeholder','Const','Relu','Reshape','Concat','Conv2D','AvgPool','MaxPool','BiasAdd','MatMul','LRN','Identity','Softmax']
    for t in s:
        if t not in ban_list:
            print(t)

def init_graph_def_placeholder_shape(graph_def, init_values):
    """
    初始化 graph_def 中 placeholder 的 shape
    eg: new_graph_def = init_graph_def_placeholder_shape(old_graph_def,
                                                        {
                                                          "placeholder1": Tensor1.get_shape(),
                                                          "placeholder2": Tensor2.get_shape(),
                                                         })
    Args:
        graphd_ef: 原始 graph_def
        init_values: 初始化参数， eg {"placeholder_name_1": shape}

    Returns: 初始化后的 graph_def
    """
    for node in graph_def.node:
        if(node.op=="Placeholder"):

            if len(node.attr["shape"].shape.dim) < 1:
                assert node.name in init_values, "%s <%s>(should be inited) not find in init_values%s"%(
                                                                              node.op,
                                                                              node.name,
                                                                              [n for n in init_values])
            else:
                if node.name not in init_values:
                    continue
        else:
            continue

        if "shape" in node.attr:
            del node.attr["shape"]

        node.attr["shape"].shape.CopyFrom(init_values[node.name].as_proto())

    tmp_graph = tf.Graph()
    with tf.Session(graph=tmp_graph):
        tf.import_graph_def(graph_def, name="")

        return tf.get_default_graph().as_graph_def(add_shapes=True)

def test_GraphShape():
    '''
    测试计算graph_def中每个op的输出样式
    :return:
    '''
    from google.protobuf import text_format
    url = os.path.abspath(os.path.join(os.path.dirname(__file__), '..','data', 'tensorflow_ssd.pb'))
    graph_def = load_pbfile(url, True)
    placeholder_name_list = []
    for node in graph_def.node:
        if node.op == 'Placeholder':
            placeholder_name_list.append(node.name)
            #print('name: %s, shape: %s'%(node.name,node.attr["shape"].shape.dim))
            #print(type(node.attr["shape"].shape.dim))
            if len(node.attr['shape'].shape.dim) >= 1:
                print([int(i.split(':')[1]) for i in repr(node.attr['shape'].shape.dim)[1:-1].split(',')])
    '''
    init_value = dict()
    for key in placeholder_name_list:
        init_value[key] = tf.ones([6,1000,1000,3],tf.int32).get_shape()
    new_graph_def = init_graph_def_placeholder_shape(graph_def, init_value)
    graph = tf.Graph()
    with graph.as_default():
        tf.import_graph_def(new_graph_def, name='')
    for op in graph.get_operations():
        #print(op.type)
        inputs = []
        for i in op.inputs:
            inputs.append([int(j) for j in i.shape])
        outs = []
        for i in op.outputs:
            outs.append([int(j) for j in i.shape])
        #print('inputs: %s'%(str(inputs)))
        #print('outputs: %s'%(str(outs)))
    '''

def test():
    url = os.path.abspath(\
        os.path.join(os.path.dirname(__file__), '..', 'tensorflow_model_test', 'tensorflow_inception_graph.pb'))
    graph = load_pbfile(url, False)
    with tf.Session(graph=graph) as sess:
        for op in graph.get_operations():
            if op.type=='Conv2D':
                print(op.type)
                for i in op.inputs:
                    print(tf.shape(i).eval()[0])

def get_placehoder_prevalue():
    url = os.path.abspath( \
        os.path.join(os.path.dirname(__file__), '..', 'tensorflow_model_test', 'tensorflow_inception_graph.pb'))
    graph_def = load_pbfile(url, True)
    from ml_feature.util.QtDialog import PlaceholderDialog
    from PyQt5.QtWidgets import QMessageBox
    placeholder_name_list = []
    for node in graph_def.node:
        if node.op == 'Placeholder':
            placeholder_name_list.append(node.name)
    flag = True
    while flag:
        try:
            print('try')
            init_value = dict()
            for key in placeholder_name_list:
                dialog = input('list:')
                arr = dialog.split(',')
                val = [int(i) for i in arr]
                init_value[key] = tf.ones(val,tf.int32).get_shape()
            print('insert shape')
            new_graph_def = init_graph_def_placeholder_shape(graph_def, init_value)
            print('over')
        except Exception as e :
            print(e)
            flag = True
        else:
            flag = False
    graph = tf.Graph()
    with graph.as_default():
        tf.import_graph_def(new_graph_def, name='')
    for op in graph.get_operations():
        print(op.type)
        inputs = []
        for i in op.inputs:
            inputs.append([int(j) for j in i.shape])
        outs = []
        for i in op.outputs:
            outs.append([int(j) for j in i.shape])
            print('inputs: %s'%(str(inputs)))
            print('outputs: %s'%(str(outs)))


def get_lstm_prevalue():
    url = os.path.abspath( \
        os.path.join(os.path.dirname(__file__), '..', 'data', 'tensorflow_lstm_graph.pb'))
    graph_def = load_pbfile(url, True)
    placeholder_name_list = []
    for node in graph_def.node:
        if node.op == 'Placeholder':
            placeholder_name_list.append(node.name)
    flag = True
    while flag:
        try:
            print('try')
            init_value = dict()
            for key in placeholder_name_list:
                dialog = input('list:')
                arr = dialog.split(',')
                val = [int(i) for i in arr]
                init_value[key] = tf.ones(val,tf.int32).get_shape()
            new_graph_def = init_graph_def_placeholder_shape(graph_def, init_value)
        except Exception as e :
            print(e)
            flag = True
        else:
            flag = False
    for node in new_graph_def.node:
        print("name: "+node.name)
        print(node.attr['shape'])
        print("inputs: %s"%node.input)
        print('------')
        print(node.attr['_output_shapes'])
        print('============')
    graph = tf.Graph()
    with graph.as_default():
        tf.import_graph_def(new_graph_def, name='')


def unknown_rank_num(graph_def):
    num = 0
    first_input_name = None
    for node in graph_def.node:
        output_shape = str(node.attr['_output_shapes'].list.shape[0]).replace('\n','').strip()
        if output_shape == 'unknown_rank: true':
            num += 1
            if not first_input_name:
                first_input_name = str(node.input[0])
    return num, first_input_name


def get_outputshape(graph_def, name):
    print('get_outputshape')
    for node in graph_def.node:
        if node.name == name:
            return node.attr['_output_shapes'].list.shape[0].dim


def lstm_init():
    url = os.path.abspath( \
        os.path.join(os.path.dirname(__file__), '..', 'data', 'tensorflow_lstm_graph.pb'))
    graph_def = load_pbfile(url, True)
    placeholder_name_list = []
    for node in graph_def.node:
        if node.op == 'Placeholder':
            placeholder_name_list.append(node.name)
    init_value = dict()
    for index, name in enumerate(placeholder_name_list):
        if index==0:
            init_value[name] = tf.ones([10,20,1],tf.int32).get_shape()
        elif index==1:
            init_value[name] = tf.ones([10,21],tf.int32).get_shape()
    new_graph_def = init_graph_def_placeholder_shape(graph_def, init_value)
    num, name = unknown_rank_num(new_graph_def)
    while num > 0 :
        print('num: %d  name: %s' % (num, name))
        shape_val = get_outputshape(new_graph_def, name)
        print(shape_val)
        if len(shape_val)>0:
            shape_val_string = str(shape_val).replace('\n','').strip()[1:-1]
            arr = [int(i.split(':')[1]) for i in shape_val_string.split(',')]
            val = tf.ones(arr, tf.int32).get_shape()
        else:
            val = tf.constant(1).get_shape()
        for node in new_graph_def.node:
            output_shape = str(node.attr['_output_shapes'].list.shape[0]).replace('\n', '').strip()
            if output_shape == 'unknown_rank: true':
                if node.op in ['Enter','Merge']:
                    continue
                if 'shape' in node.attr:
                    del node.attr['shape']
                node.attr["shape"].shape.CopyFrom(val.as_proto())
                break
        for node in new_graph_def.node:
            if '_output_shapes' in node.attr:
                del node.attr['_output_shapes']
        #print(new_graph_def)
        tmp_graph = tf.Graph()
        with tf.Session(graph=tmp_graph):
            tf.import_graph_def(new_graph_def, name="")
            new_graph_def = tf.get_default_graph().as_graph_def(add_shapes=True)
        print(new_graph_def)
        break
        num, name = unknown_rank_num(new_graph_def)
'''
    for node in new_graph_def.node:
        print('----------------')
        print('name: %s'%node.name)
        print('inputs: %s'%node.input)
        print(node.attr['_output_shapes'].list.shape[0].dim)
        print('compare: %s'%(str(node.attr['_output_shapes'].list.shape[0]).replace('\n','').strip()=='unknown_rank: true'))
        print(len(node.attr['_output_shapes'].list.shape))
        print(type(str(node.attr['_output_shapes'].list.shape[0]).replace('\n','').strip()))
        #print('--------')
'''


def getOutputshape(graph_def, name):
    '''
    获取graph_def中和name名字一样的节点的输出样式
    :param graph_def: 存储了节点信息的模型结构
    :param name: 目的节点的名字
    :return: 输出样式，list
    '''
    ans = []
    for node in graph_def.node:
        if node.name == name:
            for out in node.attr['_output_shapes'].list.shape:
                shape_string = str(out.dim).replace('\n','').strip()[1:-1]
                arr = [int(i.split(':')[1]) for i in shape_string.split(',')]
                ans.append(arr)
    return ans


if __name__=="__main__":
    url = os.path.abspath( \
        os.path.join(os.path.dirname(__file__), '..', 'data', 'tensorflow_inception_graph.pb'))
    graph_def = load_pbfile(url, True)
    placeholder_name_list = []
    for node in graph_def.node:
        if node.op == 'Placeholder':
            placeholder_name_list.append(node.name)
    init_value = dict()
    for index, name in enumerate(placeholder_name_list):
        if index == 0:
            init_value[name] = tf.ones([6, 1000, 1000, 3], tf.int32).get_shape()
    new_graph_def = init_graph_def_placeholder_shape(graph_def, init_value)

    outputurl = os.path.abspath( \
        os.path.join(os.path.dirname(__file__), '..', '..'))
    tf.train.write_graph(new_graph_def, outputurl,'inception.pbtxt')
    print('succeed')
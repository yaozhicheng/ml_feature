# coding=utf-8
# Copyright (c) 2016 Tinydot. inc.
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import tensorflow as tf
import os

def load_pbfile(url, flag=False):
    graph = tf.Graph()
    with graph.as_default():
        with open(url, 'rb') as f:
            graph_def = tf.GraphDef()
            graph_def.ParseFromString(f.read())
            tf.import_graph_def(graph_def, name='')
    if flag:
        return graph_def
    else:
        return graph


def init_graph_def_placeholder_shape(graph_def, init_values):
    """
    初始化 graph_def 中 placeholder 的 shape
    eg: new_graph_def = init_graph_def_placeholder_shape(old_graph_def,
                                                        {
                                                          "placeholder1": Tensor1.get_shape(),
                                                          "placeholder2": Tensor2.get_shape(),
                                                         })
    Args:
        graphd_ef: 原始 graph_def
        init_values: 初始化参数， eg {"placeholder_name_1": shape}

    Returns: 初始化后的 graph_def
    """
    for node in graph_def.node:
        if(node.op=="Placeholder"):

            if len(node.attr["shape"].shape.dim) < 1:
                assert node.name in init_values, "%s <%s>(should be inited) not find in init_values%s"%(
                                                                              node.op,
                                                                              node.name,
                                                                              [n for n in init_values])
            else:
                if node.name not in init_values:
                    continue
        else:
            continue

        if "shape" in node.attr:
            del node.attr["shape"]

        node.attr["shape"].shape.CopyFrom(init_values[node.name].as_proto())

    tmp_graph = tf.Graph()
    with tf.Session(graph=tmp_graph):
        tf.import_graph_def(graph_def, name="")

        return tf.get_default_graph().as_graph_def(add_shapes=True)


def unknown_rank_num(graph):
    num = 0
    first_error_op = None
    for op in graph.get_operations():
        if str(op.outputs[0].shape) == '<unknown>':
            num += 1
            if not first_error_op:
                first_error_op = op
    if first_error_op:
        first_error_op.outputs[0].set_shape(op.inputs[0].shape)
    return num


def lstm_init():
    url = os.path.abspath( \
        os.path.join(os.path.dirname(__file__), '..', '..', 'tensorflow_lstm_graph.pb'))
    old_graph_def = load_pbfile(url, True)
    placeholder_name_list = []
    for node in old_graph_def.node:
        if node.op == 'Placeholder':
            placeholder_name_list.append(node.name)
    init_value = dict()
    for index, name in enumerate(placeholder_name_list):
        if index == 0:
            init_value[name] = tf.ones([10, 20, 1], tf.int32).get_shape()
        elif index == 1:
            init_value[name] = tf.ones([10, 21], tf.int32).get_shape()
    new_graph_def = init_graph_def_placeholder_shape(old_graph_def, init_value)
    graph = tf.Graph()
    with graph.as_default():
        tf.import_graph_def(new_graph_def, name='')
    num = unknown_rank_num(graph)
    while num > 0:
        print(num)
        new_graph_def = graph.as_graph_def(add_shapes=True)
        graph = tf.Graph()
        with graph.as_default():
            tf.import_graph_def(new_graph_def, name='')
        num = unknown_rank_num(graph)


if __name__=="__main__":
    lstm_init()
# coding=utf-8
# Copyright (c) 2016 Tinydot. inc.
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import tensorflow as tf
import os

def load_pbfile(url, flag=False):
    graph = tf.Graph()
    with graph.as_default():
        with open(url, 'rb') as f:
            graph_def = tf.GraphDef()
            graph_def.ParseFromString(f.read())
            tf.import_graph_def(graph_def, name='')
    if flag:
        return graph_def
    else:
        return graph

def get_dtype_index_name_dic():
    from tensorflow.core.framework import types_pb2  ##中间生成
    ret = {}
    for d in types_pb2._DATATYPE.values:
        ret[d.number] = d.name
    return ret

def getOpdtype(filename):
    base_url = os.path.abspath(os.path.join(os.path.dirname(__file__),'..','data'))
    target_pb = [load_pbfile(os.path.join(base_url, i)) for i in filename]
    dtype = set()
    num2name = get_dtype_index_name_dic()
    for index,pb_file in enumerate(target_pb):
        print(filename[index])
        for op in pb_file.get_operations():
            if op.type not in ['Cast','Range','NoOp','SaveV2','RestoreV2']:
                try:
                    dtype_enum = op.get_attr('dtype')
                except:
                    dtype_enum = op.get_attr('T')
                if dtype_enum:
                    dtype.add(num2name[dtype_enum])
    for s in dtype:
        print(s)

def getOp(filename):
    base_url = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'data'))
    target_pb = [load_pbfile(os.path.join(base_url, i)) for i in filename]
    op_set = set()
    for pb_file in target_pb:
        for op in pb_file.get_operations():
            op_set.add(op.type+'\n')
    out_url = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..','op.txt'))
    print(len(op_set))
    with open(out_url, 'w+') as f:
        f.writelines(list(op_set))

if __name__=='__main__':
    name = ['tensorflow_inception_graph.pb','lstm.pb','ssd.pb','vgg_a.pb','vgg_d.pb','vgg_e.pb','alexnet_v2.pb']
    getOp(name)
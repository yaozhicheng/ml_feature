
Q
random_uniform/shapeConst*
dtype0*%
valueB"   ΰ   ΰ      
?
random_uniform/minConst*
valueB
 *    *
dtype0
?
random_uniform/maxConst*
valueB
 *  ?*
dtype0
r
random_uniform/RandomUniformRandomUniformrandom_uniform/shape*
seed2 *
dtype0*
T0*

seed 
J
random_uniform/subSubrandom_uniform/maxrandom_uniform/min*
T0
T
random_uniform/mulMulrandom_uniform/RandomUniformrandom_uniform/sub*
T0
F
random_uniformAddrandom_uniform/mulrandom_uniform/min*
T0
«
=vgg_19/conv1/conv1_1/weights/Initializer/random_uniform/shapeConst*%
valueB"         @   */
_class%
#!loc:@vgg_19/conv1/conv1_1/weights*
dtype0

;vgg_19/conv1/conv1_1/weights/Initializer/random_uniform/minConst*
dtype0*
valueB
 *8JΜ½*/
_class%
#!loc:@vgg_19/conv1/conv1_1/weights

;vgg_19/conv1/conv1_1/weights/Initializer/random_uniform/maxConst*
dtype0*
valueB
 *8JΜ=*/
_class%
#!loc:@vgg_19/conv1/conv1_1/weights
υ
Evgg_19/conv1/conv1_1/weights/Initializer/random_uniform/RandomUniformRandomUniform=vgg_19/conv1/conv1_1/weights/Initializer/random_uniform/shape*
dtype0*

seed *
T0*
seed2 */
_class%
#!loc:@vgg_19/conv1/conv1_1/weights
φ
;vgg_19/conv1/conv1_1/weights/Initializer/random_uniform/subSub;vgg_19/conv1/conv1_1/weights/Initializer/random_uniform/max;vgg_19/conv1/conv1_1/weights/Initializer/random_uniform/min*/
_class%
#!loc:@vgg_19/conv1/conv1_1/weights*
T0

;vgg_19/conv1/conv1_1/weights/Initializer/random_uniform/mulMulEvgg_19/conv1/conv1_1/weights/Initializer/random_uniform/RandomUniform;vgg_19/conv1/conv1_1/weights/Initializer/random_uniform/sub*/
_class%
#!loc:@vgg_19/conv1/conv1_1/weights*
T0
ς
7vgg_19/conv1/conv1_1/weights/Initializer/random_uniformAdd;vgg_19/conv1/conv1_1/weights/Initializer/random_uniform/mul;vgg_19/conv1/conv1_1/weights/Initializer/random_uniform/min*/
_class%
#!loc:@vgg_19/conv1/conv1_1/weights*
T0
©
vgg_19/conv1/conv1_1/weights
VariableV2*
shared_name *
dtype0*
shape:@*
	container */
_class%
#!loc:@vgg_19/conv1/conv1_1/weights
η
#vgg_19/conv1/conv1_1/weights/AssignAssignvgg_19/conv1/conv1_1/weights7vgg_19/conv1/conv1_1/weights/Initializer/random_uniform*/
_class%
#!loc:@vgg_19/conv1/conv1_1/weights*
T0*
validate_shape(*
use_locking(

!vgg_19/conv1/conv1_1/weights/readIdentityvgg_19/conv1/conv1_1/weights*
T0*/
_class%
#!loc:@vgg_19/conv1/conv1_1/weights

-vgg_19/conv1/conv1_1/biases/Initializer/zerosConst*
dtype0*
valueB@*    *.
_class$
" loc:@vgg_19/conv1/conv1_1/biases

vgg_19/conv1/conv1_1/biases
VariableV2*
shape:@*
shared_name *.
_class$
" loc:@vgg_19/conv1/conv1_1/biases*
dtype0*
	container 
Ϊ
"vgg_19/conv1/conv1_1/biases/AssignAssignvgg_19/conv1/conv1_1/biases-vgg_19/conv1/conv1_1/biases/Initializer/zeros*.
_class$
" loc:@vgg_19/conv1/conv1_1/biases*
T0*
validate_shape(*
use_locking(

 vgg_19/conv1/conv1_1/biases/readIdentityvgg_19/conv1/conv1_1/biases*.
_class$
" loc:@vgg_19/conv1/conv1_1/biases*
T0
c
&vgg_19/conv1/conv1_1/convolution/ShapeConst*
dtype0*%
valueB"         @   
c
.vgg_19/conv1/conv1_1/convolution/dilation_rateConst*
dtype0*
valueB"      
½
 vgg_19/conv1/conv1_1/convolutionConv2Drandom_uniform!vgg_19/conv1/conv1_1/weights/read*
use_cudnn_on_gpu(*
T0*
strides
*
data_formatNHWC*
paddingSAME

vgg_19/conv1/conv1_1/BiasAddBiasAdd vgg_19/conv1/conv1_1/convolution vgg_19/conv1/conv1_1/biases/read*
T0*
data_formatNHWC
H
vgg_19/conv1/conv1_1/ReluReluvgg_19/conv1/conv1_1/BiasAdd*
T0
«
=vgg_19/conv1/conv1_2/weights/Initializer/random_uniform/shapeConst*%
valueB"      @   @   */
_class%
#!loc:@vgg_19/conv1/conv1_2/weights*
dtype0

;vgg_19/conv1/conv1_2/weights/Initializer/random_uniform/minConst*
dtype0*
valueB
 *:Ν½*/
_class%
#!loc:@vgg_19/conv1/conv1_2/weights

;vgg_19/conv1/conv1_2/weights/Initializer/random_uniform/maxConst*
valueB
 *:Ν=*/
_class%
#!loc:@vgg_19/conv1/conv1_2/weights*
dtype0
υ
Evgg_19/conv1/conv1_2/weights/Initializer/random_uniform/RandomUniformRandomUniform=vgg_19/conv1/conv1_2/weights/Initializer/random_uniform/shape*
dtype0*

seed *
T0*
seed2 */
_class%
#!loc:@vgg_19/conv1/conv1_2/weights
φ
;vgg_19/conv1/conv1_2/weights/Initializer/random_uniform/subSub;vgg_19/conv1/conv1_2/weights/Initializer/random_uniform/max;vgg_19/conv1/conv1_2/weights/Initializer/random_uniform/min*
T0*/
_class%
#!loc:@vgg_19/conv1/conv1_2/weights

;vgg_19/conv1/conv1_2/weights/Initializer/random_uniform/mulMulEvgg_19/conv1/conv1_2/weights/Initializer/random_uniform/RandomUniform;vgg_19/conv1/conv1_2/weights/Initializer/random_uniform/sub*
T0*/
_class%
#!loc:@vgg_19/conv1/conv1_2/weights
ς
7vgg_19/conv1/conv1_2/weights/Initializer/random_uniformAdd;vgg_19/conv1/conv1_2/weights/Initializer/random_uniform/mul;vgg_19/conv1/conv1_2/weights/Initializer/random_uniform/min*
T0*/
_class%
#!loc:@vgg_19/conv1/conv1_2/weights
©
vgg_19/conv1/conv1_2/weights
VariableV2*/
_class%
#!loc:@vgg_19/conv1/conv1_2/weights*
	container *
shape:@@*
dtype0*
shared_name 
η
#vgg_19/conv1/conv1_2/weights/AssignAssignvgg_19/conv1/conv1_2/weights7vgg_19/conv1/conv1_2/weights/Initializer/random_uniform*
use_locking(*
validate_shape(*
T0*/
_class%
#!loc:@vgg_19/conv1/conv1_2/weights

!vgg_19/conv1/conv1_2/weights/readIdentityvgg_19/conv1/conv1_2/weights*/
_class%
#!loc:@vgg_19/conv1/conv1_2/weights*
T0

-vgg_19/conv1/conv1_2/biases/Initializer/zerosConst*
dtype0*
valueB@*    *.
_class$
" loc:@vgg_19/conv1/conv1_2/biases

vgg_19/conv1/conv1_2/biases
VariableV2*
	container *
dtype0*.
_class$
" loc:@vgg_19/conv1/conv1_2/biases*
shared_name *
shape:@
Ϊ
"vgg_19/conv1/conv1_2/biases/AssignAssignvgg_19/conv1/conv1_2/biases-vgg_19/conv1/conv1_2/biases/Initializer/zeros*.
_class$
" loc:@vgg_19/conv1/conv1_2/biases*
T0*
validate_shape(*
use_locking(

 vgg_19/conv1/conv1_2/biases/readIdentityvgg_19/conv1/conv1_2/biases*
T0*.
_class$
" loc:@vgg_19/conv1/conv1_2/biases
c
&vgg_19/conv1/conv1_2/convolution/ShapeConst*
dtype0*%
valueB"      @   @   
c
.vgg_19/conv1/conv1_2/convolution/dilation_rateConst*
dtype0*
valueB"      
Θ
 vgg_19/conv1/conv1_2/convolutionConv2Dvgg_19/conv1/conv1_1/Relu!vgg_19/conv1/conv1_2/weights/read*
paddingSAME*
T0*
strides
*
data_formatNHWC*
use_cudnn_on_gpu(

vgg_19/conv1/conv1_2/BiasAddBiasAdd vgg_19/conv1/conv1_2/convolution vgg_19/conv1/conv1_2/biases/read*
T0*
data_formatNHWC
H
vgg_19/conv1/conv1_2/ReluReluvgg_19/conv1/conv1_2/BiasAdd*
T0

vgg_19/pool1/MaxPoolMaxPoolvgg_19/conv1/conv1_2/Relu*
strides
*
data_formatNHWC*
T0*
paddingVALID*
ksize

«
=vgg_19/conv2/conv2_1/weights/Initializer/random_uniform/shapeConst*
dtype0*%
valueB"      @      */
_class%
#!loc:@vgg_19/conv2/conv2_1/weights

;vgg_19/conv2/conv2_1/weights/Initializer/random_uniform/minConst*
dtype0*
valueB
 *ο[q½*/
_class%
#!loc:@vgg_19/conv2/conv2_1/weights

;vgg_19/conv2/conv2_1/weights/Initializer/random_uniform/maxConst*
dtype0*
valueB
 *ο[q=*/
_class%
#!loc:@vgg_19/conv2/conv2_1/weights
υ
Evgg_19/conv2/conv2_1/weights/Initializer/random_uniform/RandomUniformRandomUniform=vgg_19/conv2/conv2_1/weights/Initializer/random_uniform/shape*
dtype0*

seed *
T0*
seed2 */
_class%
#!loc:@vgg_19/conv2/conv2_1/weights
φ
;vgg_19/conv2/conv2_1/weights/Initializer/random_uniform/subSub;vgg_19/conv2/conv2_1/weights/Initializer/random_uniform/max;vgg_19/conv2/conv2_1/weights/Initializer/random_uniform/min*
T0*/
_class%
#!loc:@vgg_19/conv2/conv2_1/weights

;vgg_19/conv2/conv2_1/weights/Initializer/random_uniform/mulMulEvgg_19/conv2/conv2_1/weights/Initializer/random_uniform/RandomUniform;vgg_19/conv2/conv2_1/weights/Initializer/random_uniform/sub*
T0*/
_class%
#!loc:@vgg_19/conv2/conv2_1/weights
ς
7vgg_19/conv2/conv2_1/weights/Initializer/random_uniformAdd;vgg_19/conv2/conv2_1/weights/Initializer/random_uniform/mul;vgg_19/conv2/conv2_1/weights/Initializer/random_uniform/min*
T0*/
_class%
#!loc:@vgg_19/conv2/conv2_1/weights
ͺ
vgg_19/conv2/conv2_1/weights
VariableV2*
shape:@*
shared_name */
_class%
#!loc:@vgg_19/conv2/conv2_1/weights*
dtype0*
	container 
η
#vgg_19/conv2/conv2_1/weights/AssignAssignvgg_19/conv2/conv2_1/weights7vgg_19/conv2/conv2_1/weights/Initializer/random_uniform*
use_locking(*
validate_shape(*
T0*/
_class%
#!loc:@vgg_19/conv2/conv2_1/weights

!vgg_19/conv2/conv2_1/weights/readIdentityvgg_19/conv2/conv2_1/weights*
T0*/
_class%
#!loc:@vgg_19/conv2/conv2_1/weights

-vgg_19/conv2/conv2_1/biases/Initializer/zerosConst*
valueB*    *.
_class$
" loc:@vgg_19/conv2/conv2_1/biases*
dtype0

vgg_19/conv2/conv2_1/biases
VariableV2*.
_class$
" loc:@vgg_19/conv2/conv2_1/biases*
	container *
shape:*
dtype0*
shared_name 
Ϊ
"vgg_19/conv2/conv2_1/biases/AssignAssignvgg_19/conv2/conv2_1/biases-vgg_19/conv2/conv2_1/biases/Initializer/zeros*
use_locking(*
T0*.
_class$
" loc:@vgg_19/conv2/conv2_1/biases*
validate_shape(

 vgg_19/conv2/conv2_1/biases/readIdentityvgg_19/conv2/conv2_1/biases*
T0*.
_class$
" loc:@vgg_19/conv2/conv2_1/biases
c
&vgg_19/conv2/conv2_1/convolution/ShapeConst*
dtype0*%
valueB"      @      
c
.vgg_19/conv2/conv2_1/convolution/dilation_rateConst*
valueB"      *
dtype0
Γ
 vgg_19/conv2/conv2_1/convolutionConv2Dvgg_19/pool1/MaxPool!vgg_19/conv2/conv2_1/weights/read*
paddingSAME*
strides
*
data_formatNHWC*
T0*
use_cudnn_on_gpu(

vgg_19/conv2/conv2_1/BiasAddBiasAdd vgg_19/conv2/conv2_1/convolution vgg_19/conv2/conv2_1/biases/read*
data_formatNHWC*
T0
H
vgg_19/conv2/conv2_1/ReluReluvgg_19/conv2/conv2_1/BiasAdd*
T0
«
=vgg_19/conv2/conv2_2/weights/Initializer/random_uniform/shapeConst*
dtype0*%
valueB"            */
_class%
#!loc:@vgg_19/conv2/conv2_2/weights

;vgg_19/conv2/conv2_2/weights/Initializer/random_uniform/minConst*
dtype0*
valueB
 *μQ½*/
_class%
#!loc:@vgg_19/conv2/conv2_2/weights

;vgg_19/conv2/conv2_2/weights/Initializer/random_uniform/maxConst*
dtype0*
valueB
 *μQ=*/
_class%
#!loc:@vgg_19/conv2/conv2_2/weights
υ
Evgg_19/conv2/conv2_2/weights/Initializer/random_uniform/RandomUniformRandomUniform=vgg_19/conv2/conv2_2/weights/Initializer/random_uniform/shape*

seed *
T0*/
_class%
#!loc:@vgg_19/conv2/conv2_2/weights*
dtype0*
seed2 
φ
;vgg_19/conv2/conv2_2/weights/Initializer/random_uniform/subSub;vgg_19/conv2/conv2_2/weights/Initializer/random_uniform/max;vgg_19/conv2/conv2_2/weights/Initializer/random_uniform/min*
T0*/
_class%
#!loc:@vgg_19/conv2/conv2_2/weights

;vgg_19/conv2/conv2_2/weights/Initializer/random_uniform/mulMulEvgg_19/conv2/conv2_2/weights/Initializer/random_uniform/RandomUniform;vgg_19/conv2/conv2_2/weights/Initializer/random_uniform/sub*
T0*/
_class%
#!loc:@vgg_19/conv2/conv2_2/weights
ς
7vgg_19/conv2/conv2_2/weights/Initializer/random_uniformAdd;vgg_19/conv2/conv2_2/weights/Initializer/random_uniform/mul;vgg_19/conv2/conv2_2/weights/Initializer/random_uniform/min*
T0*/
_class%
#!loc:@vgg_19/conv2/conv2_2/weights
«
vgg_19/conv2/conv2_2/weights
VariableV2*
shape:*
shared_name */
_class%
#!loc:@vgg_19/conv2/conv2_2/weights*
dtype0*
	container 
η
#vgg_19/conv2/conv2_2/weights/AssignAssignvgg_19/conv2/conv2_2/weights7vgg_19/conv2/conv2_2/weights/Initializer/random_uniform*
validate_shape(*/
_class%
#!loc:@vgg_19/conv2/conv2_2/weights*
T0*
use_locking(

!vgg_19/conv2/conv2_2/weights/readIdentityvgg_19/conv2/conv2_2/weights*/
_class%
#!loc:@vgg_19/conv2/conv2_2/weights*
T0

-vgg_19/conv2/conv2_2/biases/Initializer/zerosConst*
valueB*    *.
_class$
" loc:@vgg_19/conv2/conv2_2/biases*
dtype0

vgg_19/conv2/conv2_2/biases
VariableV2*
shared_name *
dtype0*
shape:*
	container *.
_class$
" loc:@vgg_19/conv2/conv2_2/biases
Ϊ
"vgg_19/conv2/conv2_2/biases/AssignAssignvgg_19/conv2/conv2_2/biases-vgg_19/conv2/conv2_2/biases/Initializer/zeros*.
_class$
" loc:@vgg_19/conv2/conv2_2/biases*
T0*
validate_shape(*
use_locking(

 vgg_19/conv2/conv2_2/biases/readIdentityvgg_19/conv2/conv2_2/biases*
T0*.
_class$
" loc:@vgg_19/conv2/conv2_2/biases
c
&vgg_19/conv2/conv2_2/convolution/ShapeConst*
dtype0*%
valueB"            
c
.vgg_19/conv2/conv2_2/convolution/dilation_rateConst*
valueB"      *
dtype0
Θ
 vgg_19/conv2/conv2_2/convolutionConv2Dvgg_19/conv2/conv2_1/Relu!vgg_19/conv2/conv2_2/weights/read*
paddingSAME*
strides
*
data_formatNHWC*
T0*
use_cudnn_on_gpu(

vgg_19/conv2/conv2_2/BiasAddBiasAdd vgg_19/conv2/conv2_2/convolution vgg_19/conv2/conv2_2/biases/read*
T0*
data_formatNHWC
H
vgg_19/conv2/conv2_2/ReluReluvgg_19/conv2/conv2_2/BiasAdd*
T0

vgg_19/pool2/MaxPoolMaxPoolvgg_19/conv2/conv2_2/Relu*
ksize
*
paddingVALID*
T0*
strides
*
data_formatNHWC
«
=vgg_19/conv3/conv3_1/weights/Initializer/random_uniform/shapeConst*
dtype0*%
valueB"            */
_class%
#!loc:@vgg_19/conv3/conv3_1/weights

;vgg_19/conv3/conv3_1/weights/Initializer/random_uniform/minConst*
dtype0*
valueB
 *«ͺ*½*/
_class%
#!loc:@vgg_19/conv3/conv3_1/weights

;vgg_19/conv3/conv3_1/weights/Initializer/random_uniform/maxConst*
valueB
 *«ͺ*=*/
_class%
#!loc:@vgg_19/conv3/conv3_1/weights*
dtype0
υ
Evgg_19/conv3/conv3_1/weights/Initializer/random_uniform/RandomUniformRandomUniform=vgg_19/conv3/conv3_1/weights/Initializer/random_uniform/shape*
dtype0*

seed *
T0*
seed2 */
_class%
#!loc:@vgg_19/conv3/conv3_1/weights
φ
;vgg_19/conv3/conv3_1/weights/Initializer/random_uniform/subSub;vgg_19/conv3/conv3_1/weights/Initializer/random_uniform/max;vgg_19/conv3/conv3_1/weights/Initializer/random_uniform/min*
T0*/
_class%
#!loc:@vgg_19/conv3/conv3_1/weights

;vgg_19/conv3/conv3_1/weights/Initializer/random_uniform/mulMulEvgg_19/conv3/conv3_1/weights/Initializer/random_uniform/RandomUniform;vgg_19/conv3/conv3_1/weights/Initializer/random_uniform/sub*/
_class%
#!loc:@vgg_19/conv3/conv3_1/weights*
T0
ς
7vgg_19/conv3/conv3_1/weights/Initializer/random_uniformAdd;vgg_19/conv3/conv3_1/weights/Initializer/random_uniform/mul;vgg_19/conv3/conv3_1/weights/Initializer/random_uniform/min*
T0*/
_class%
#!loc:@vgg_19/conv3/conv3_1/weights
«
vgg_19/conv3/conv3_1/weights
VariableV2*/
_class%
#!loc:@vgg_19/conv3/conv3_1/weights*
	container *
shape:*
dtype0*
shared_name 
η
#vgg_19/conv3/conv3_1/weights/AssignAssignvgg_19/conv3/conv3_1/weights7vgg_19/conv3/conv3_1/weights/Initializer/random_uniform*
validate_shape(*/
_class%
#!loc:@vgg_19/conv3/conv3_1/weights*
T0*
use_locking(

!vgg_19/conv3/conv3_1/weights/readIdentityvgg_19/conv3/conv3_1/weights*
T0*/
_class%
#!loc:@vgg_19/conv3/conv3_1/weights

-vgg_19/conv3/conv3_1/biases/Initializer/zerosConst*
valueB*    *.
_class$
" loc:@vgg_19/conv3/conv3_1/biases*
dtype0

vgg_19/conv3/conv3_1/biases
VariableV2*
shared_name *
dtype0*
shape:*
	container *.
_class$
" loc:@vgg_19/conv3/conv3_1/biases
Ϊ
"vgg_19/conv3/conv3_1/biases/AssignAssignvgg_19/conv3/conv3_1/biases-vgg_19/conv3/conv3_1/biases/Initializer/zeros*.
_class$
" loc:@vgg_19/conv3/conv3_1/biases*
T0*
validate_shape(*
use_locking(

 vgg_19/conv3/conv3_1/biases/readIdentityvgg_19/conv3/conv3_1/biases*.
_class$
" loc:@vgg_19/conv3/conv3_1/biases*
T0
c
&vgg_19/conv3/conv3_1/convolution/ShapeConst*%
valueB"            *
dtype0
c
.vgg_19/conv3/conv3_1/convolution/dilation_rateConst*
valueB"      *
dtype0
Γ
 vgg_19/conv3/conv3_1/convolutionConv2Dvgg_19/pool2/MaxPool!vgg_19/conv3/conv3_1/weights/read*
paddingSAME*
strides
*
data_formatNHWC*
T0*
use_cudnn_on_gpu(

vgg_19/conv3/conv3_1/BiasAddBiasAdd vgg_19/conv3/conv3_1/convolution vgg_19/conv3/conv3_1/biases/read*
T0*
data_formatNHWC
H
vgg_19/conv3/conv3_1/ReluReluvgg_19/conv3/conv3_1/BiasAdd*
T0
«
=vgg_19/conv3/conv3_2/weights/Initializer/random_uniform/shapeConst*
dtype0*%
valueB"            */
_class%
#!loc:@vgg_19/conv3/conv3_2/weights

;vgg_19/conv3/conv3_2/weights/Initializer/random_uniform/minConst*
dtype0*
valueB
 *:Ν½*/
_class%
#!loc:@vgg_19/conv3/conv3_2/weights

;vgg_19/conv3/conv3_2/weights/Initializer/random_uniform/maxConst*
valueB
 *:Ν=*/
_class%
#!loc:@vgg_19/conv3/conv3_2/weights*
dtype0
υ
Evgg_19/conv3/conv3_2/weights/Initializer/random_uniform/RandomUniformRandomUniform=vgg_19/conv3/conv3_2/weights/Initializer/random_uniform/shape*/
_class%
#!loc:@vgg_19/conv3/conv3_2/weights*
seed2 *
T0*

seed *
dtype0
φ
;vgg_19/conv3/conv3_2/weights/Initializer/random_uniform/subSub;vgg_19/conv3/conv3_2/weights/Initializer/random_uniform/max;vgg_19/conv3/conv3_2/weights/Initializer/random_uniform/min*
T0*/
_class%
#!loc:@vgg_19/conv3/conv3_2/weights

;vgg_19/conv3/conv3_2/weights/Initializer/random_uniform/mulMulEvgg_19/conv3/conv3_2/weights/Initializer/random_uniform/RandomUniform;vgg_19/conv3/conv3_2/weights/Initializer/random_uniform/sub*
T0*/
_class%
#!loc:@vgg_19/conv3/conv3_2/weights
ς
7vgg_19/conv3/conv3_2/weights/Initializer/random_uniformAdd;vgg_19/conv3/conv3_2/weights/Initializer/random_uniform/mul;vgg_19/conv3/conv3_2/weights/Initializer/random_uniform/min*
T0*/
_class%
#!loc:@vgg_19/conv3/conv3_2/weights
«
vgg_19/conv3/conv3_2/weights
VariableV2*
shape:*
shared_name */
_class%
#!loc:@vgg_19/conv3/conv3_2/weights*
dtype0*
	container 
η
#vgg_19/conv3/conv3_2/weights/AssignAssignvgg_19/conv3/conv3_2/weights7vgg_19/conv3/conv3_2/weights/Initializer/random_uniform*
validate_shape(*/
_class%
#!loc:@vgg_19/conv3/conv3_2/weights*
T0*
use_locking(

!vgg_19/conv3/conv3_2/weights/readIdentityvgg_19/conv3/conv3_2/weights*/
_class%
#!loc:@vgg_19/conv3/conv3_2/weights*
T0

-vgg_19/conv3/conv3_2/biases/Initializer/zerosConst*
dtype0*
valueB*    *.
_class$
" loc:@vgg_19/conv3/conv3_2/biases

vgg_19/conv3/conv3_2/biases
VariableV2*
	container *
dtype0*.
_class$
" loc:@vgg_19/conv3/conv3_2/biases*
shared_name *
shape:
Ϊ
"vgg_19/conv3/conv3_2/biases/AssignAssignvgg_19/conv3/conv3_2/biases-vgg_19/conv3/conv3_2/biases/Initializer/zeros*
validate_shape(*.
_class$
" loc:@vgg_19/conv3/conv3_2/biases*
T0*
use_locking(

 vgg_19/conv3/conv3_2/biases/readIdentityvgg_19/conv3/conv3_2/biases*.
_class$
" loc:@vgg_19/conv3/conv3_2/biases*
T0
c
&vgg_19/conv3/conv3_2/convolution/ShapeConst*
dtype0*%
valueB"            
c
.vgg_19/conv3/conv3_2/convolution/dilation_rateConst*
dtype0*
valueB"      
Θ
 vgg_19/conv3/conv3_2/convolutionConv2Dvgg_19/conv3/conv3_1/Relu!vgg_19/conv3/conv3_2/weights/read*
paddingSAME*
strides
*
data_formatNHWC*
T0*
use_cudnn_on_gpu(

vgg_19/conv3/conv3_2/BiasAddBiasAdd vgg_19/conv3/conv3_2/convolution vgg_19/conv3/conv3_2/biases/read*
T0*
data_formatNHWC
H
vgg_19/conv3/conv3_2/ReluReluvgg_19/conv3/conv3_2/BiasAdd*
T0
«
=vgg_19/conv3/conv3_3/weights/Initializer/random_uniform/shapeConst*%
valueB"            */
_class%
#!loc:@vgg_19/conv3/conv3_3/weights*
dtype0

;vgg_19/conv3/conv3_3/weights/Initializer/random_uniform/minConst*
valueB
 *:Ν½*/
_class%
#!loc:@vgg_19/conv3/conv3_3/weights*
dtype0

;vgg_19/conv3/conv3_3/weights/Initializer/random_uniform/maxConst*
dtype0*
valueB
 *:Ν=*/
_class%
#!loc:@vgg_19/conv3/conv3_3/weights
υ
Evgg_19/conv3/conv3_3/weights/Initializer/random_uniform/RandomUniformRandomUniform=vgg_19/conv3/conv3_3/weights/Initializer/random_uniform/shape*
seed2 *
dtype0*/
_class%
#!loc:@vgg_19/conv3/conv3_3/weights*
T0*

seed 
φ
;vgg_19/conv3/conv3_3/weights/Initializer/random_uniform/subSub;vgg_19/conv3/conv3_3/weights/Initializer/random_uniform/max;vgg_19/conv3/conv3_3/weights/Initializer/random_uniform/min*
T0*/
_class%
#!loc:@vgg_19/conv3/conv3_3/weights

;vgg_19/conv3/conv3_3/weights/Initializer/random_uniform/mulMulEvgg_19/conv3/conv3_3/weights/Initializer/random_uniform/RandomUniform;vgg_19/conv3/conv3_3/weights/Initializer/random_uniform/sub*/
_class%
#!loc:@vgg_19/conv3/conv3_3/weights*
T0
ς
7vgg_19/conv3/conv3_3/weights/Initializer/random_uniformAdd;vgg_19/conv3/conv3_3/weights/Initializer/random_uniform/mul;vgg_19/conv3/conv3_3/weights/Initializer/random_uniform/min*
T0*/
_class%
#!loc:@vgg_19/conv3/conv3_3/weights
«
vgg_19/conv3/conv3_3/weights
VariableV2*
shared_name *
dtype0*
shape:*
	container */
_class%
#!loc:@vgg_19/conv3/conv3_3/weights
η
#vgg_19/conv3/conv3_3/weights/AssignAssignvgg_19/conv3/conv3_3/weights7vgg_19/conv3/conv3_3/weights/Initializer/random_uniform*/
_class%
#!loc:@vgg_19/conv3/conv3_3/weights*
T0*
validate_shape(*
use_locking(

!vgg_19/conv3/conv3_3/weights/readIdentityvgg_19/conv3/conv3_3/weights*
T0*/
_class%
#!loc:@vgg_19/conv3/conv3_3/weights

-vgg_19/conv3/conv3_3/biases/Initializer/zerosConst*
valueB*    *.
_class$
" loc:@vgg_19/conv3/conv3_3/biases*
dtype0

vgg_19/conv3/conv3_3/biases
VariableV2*
shape:*
shared_name *.
_class$
" loc:@vgg_19/conv3/conv3_3/biases*
dtype0*
	container 
Ϊ
"vgg_19/conv3/conv3_3/biases/AssignAssignvgg_19/conv3/conv3_3/biases-vgg_19/conv3/conv3_3/biases/Initializer/zeros*
use_locking(*
validate_shape(*
T0*.
_class$
" loc:@vgg_19/conv3/conv3_3/biases

 vgg_19/conv3/conv3_3/biases/readIdentityvgg_19/conv3/conv3_3/biases*
T0*.
_class$
" loc:@vgg_19/conv3/conv3_3/biases
c
&vgg_19/conv3/conv3_3/convolution/ShapeConst*%
valueB"            *
dtype0
c
.vgg_19/conv3/conv3_3/convolution/dilation_rateConst*
dtype0*
valueB"      
Θ
 vgg_19/conv3/conv3_3/convolutionConv2Dvgg_19/conv3/conv3_2/Relu!vgg_19/conv3/conv3_3/weights/read*
use_cudnn_on_gpu(*
T0*
strides
*
data_formatNHWC*
paddingSAME

vgg_19/conv3/conv3_3/BiasAddBiasAdd vgg_19/conv3/conv3_3/convolution vgg_19/conv3/conv3_3/biases/read*
T0*
data_formatNHWC
H
vgg_19/conv3/conv3_3/ReluReluvgg_19/conv3/conv3_3/BiasAdd*
T0
«
=vgg_19/conv3/conv3_4/weights/Initializer/random_uniform/shapeConst*%
valueB"            */
_class%
#!loc:@vgg_19/conv3/conv3_4/weights*
dtype0

;vgg_19/conv3/conv3_4/weights/Initializer/random_uniform/minConst*
valueB
 *:Ν½*/
_class%
#!loc:@vgg_19/conv3/conv3_4/weights*
dtype0

;vgg_19/conv3/conv3_4/weights/Initializer/random_uniform/maxConst*
valueB
 *:Ν=*/
_class%
#!loc:@vgg_19/conv3/conv3_4/weights*
dtype0
υ
Evgg_19/conv3/conv3_4/weights/Initializer/random_uniform/RandomUniformRandomUniform=vgg_19/conv3/conv3_4/weights/Initializer/random_uniform/shape*
dtype0*

seed *
T0*
seed2 */
_class%
#!loc:@vgg_19/conv3/conv3_4/weights
φ
;vgg_19/conv3/conv3_4/weights/Initializer/random_uniform/subSub;vgg_19/conv3/conv3_4/weights/Initializer/random_uniform/max;vgg_19/conv3/conv3_4/weights/Initializer/random_uniform/min*
T0*/
_class%
#!loc:@vgg_19/conv3/conv3_4/weights

;vgg_19/conv3/conv3_4/weights/Initializer/random_uniform/mulMulEvgg_19/conv3/conv3_4/weights/Initializer/random_uniform/RandomUniform;vgg_19/conv3/conv3_4/weights/Initializer/random_uniform/sub*/
_class%
#!loc:@vgg_19/conv3/conv3_4/weights*
T0
ς
7vgg_19/conv3/conv3_4/weights/Initializer/random_uniformAdd;vgg_19/conv3/conv3_4/weights/Initializer/random_uniform/mul;vgg_19/conv3/conv3_4/weights/Initializer/random_uniform/min*
T0*/
_class%
#!loc:@vgg_19/conv3/conv3_4/weights
«
vgg_19/conv3/conv3_4/weights
VariableV2*
	container *
dtype0*/
_class%
#!loc:@vgg_19/conv3/conv3_4/weights*
shared_name *
shape:
η
#vgg_19/conv3/conv3_4/weights/AssignAssignvgg_19/conv3/conv3_4/weights7vgg_19/conv3/conv3_4/weights/Initializer/random_uniform*
validate_shape(*/
_class%
#!loc:@vgg_19/conv3/conv3_4/weights*
T0*
use_locking(

!vgg_19/conv3/conv3_4/weights/readIdentityvgg_19/conv3/conv3_4/weights*/
_class%
#!loc:@vgg_19/conv3/conv3_4/weights*
T0

-vgg_19/conv3/conv3_4/biases/Initializer/zerosConst*
dtype0*
valueB*    *.
_class$
" loc:@vgg_19/conv3/conv3_4/biases

vgg_19/conv3/conv3_4/biases
VariableV2*
shared_name *
dtype0*
shape:*
	container *.
_class$
" loc:@vgg_19/conv3/conv3_4/biases
Ϊ
"vgg_19/conv3/conv3_4/biases/AssignAssignvgg_19/conv3/conv3_4/biases-vgg_19/conv3/conv3_4/biases/Initializer/zeros*
use_locking(*
validate_shape(*
T0*.
_class$
" loc:@vgg_19/conv3/conv3_4/biases

 vgg_19/conv3/conv3_4/biases/readIdentityvgg_19/conv3/conv3_4/biases*
T0*.
_class$
" loc:@vgg_19/conv3/conv3_4/biases
c
&vgg_19/conv3/conv3_4/convolution/ShapeConst*%
valueB"            *
dtype0
c
.vgg_19/conv3/conv3_4/convolution/dilation_rateConst*
valueB"      *
dtype0
Θ
 vgg_19/conv3/conv3_4/convolutionConv2Dvgg_19/conv3/conv3_3/Relu!vgg_19/conv3/conv3_4/weights/read*
paddingSAME*
strides
*
data_formatNHWC*
T0*
use_cudnn_on_gpu(

vgg_19/conv3/conv3_4/BiasAddBiasAdd vgg_19/conv3/conv3_4/convolution vgg_19/conv3/conv3_4/biases/read*
data_formatNHWC*
T0
H
vgg_19/conv3/conv3_4/ReluReluvgg_19/conv3/conv3_4/BiasAdd*
T0

vgg_19/pool3/MaxPoolMaxPoolvgg_19/conv3/conv3_4/Relu*
ksize
*
paddingVALID*
T0*
strides
*
data_formatNHWC
«
=vgg_19/conv4/conv4_1/weights/Initializer/random_uniform/shapeConst*
dtype0*%
valueB"            */
_class%
#!loc:@vgg_19/conv4/conv4_1/weights

;vgg_19/conv4/conv4_1/weights/Initializer/random_uniform/minConst*
valueB
 *ο[ρΌ*/
_class%
#!loc:@vgg_19/conv4/conv4_1/weights*
dtype0

;vgg_19/conv4/conv4_1/weights/Initializer/random_uniform/maxConst*
dtype0*
valueB
 *ο[ρ<*/
_class%
#!loc:@vgg_19/conv4/conv4_1/weights
υ
Evgg_19/conv4/conv4_1/weights/Initializer/random_uniform/RandomUniformRandomUniform=vgg_19/conv4/conv4_1/weights/Initializer/random_uniform/shape*

seed *
T0*/
_class%
#!loc:@vgg_19/conv4/conv4_1/weights*
dtype0*
seed2 
φ
;vgg_19/conv4/conv4_1/weights/Initializer/random_uniform/subSub;vgg_19/conv4/conv4_1/weights/Initializer/random_uniform/max;vgg_19/conv4/conv4_1/weights/Initializer/random_uniform/min*/
_class%
#!loc:@vgg_19/conv4/conv4_1/weights*
T0

;vgg_19/conv4/conv4_1/weights/Initializer/random_uniform/mulMulEvgg_19/conv4/conv4_1/weights/Initializer/random_uniform/RandomUniform;vgg_19/conv4/conv4_1/weights/Initializer/random_uniform/sub*
T0*/
_class%
#!loc:@vgg_19/conv4/conv4_1/weights
ς
7vgg_19/conv4/conv4_1/weights/Initializer/random_uniformAdd;vgg_19/conv4/conv4_1/weights/Initializer/random_uniform/mul;vgg_19/conv4/conv4_1/weights/Initializer/random_uniform/min*
T0*/
_class%
#!loc:@vgg_19/conv4/conv4_1/weights
«
vgg_19/conv4/conv4_1/weights
VariableV2*
	container *
dtype0*/
_class%
#!loc:@vgg_19/conv4/conv4_1/weights*
shared_name *
shape:
η
#vgg_19/conv4/conv4_1/weights/AssignAssignvgg_19/conv4/conv4_1/weights7vgg_19/conv4/conv4_1/weights/Initializer/random_uniform*
validate_shape(*/
_class%
#!loc:@vgg_19/conv4/conv4_1/weights*
T0*
use_locking(

!vgg_19/conv4/conv4_1/weights/readIdentityvgg_19/conv4/conv4_1/weights*/
_class%
#!loc:@vgg_19/conv4/conv4_1/weights*
T0

-vgg_19/conv4/conv4_1/biases/Initializer/zerosConst*
dtype0*
valueB*    *.
_class$
" loc:@vgg_19/conv4/conv4_1/biases

vgg_19/conv4/conv4_1/biases
VariableV2*
shared_name *
dtype0*
shape:*
	container *.
_class$
" loc:@vgg_19/conv4/conv4_1/biases
Ϊ
"vgg_19/conv4/conv4_1/biases/AssignAssignvgg_19/conv4/conv4_1/biases-vgg_19/conv4/conv4_1/biases/Initializer/zeros*
use_locking(*
T0*.
_class$
" loc:@vgg_19/conv4/conv4_1/biases*
validate_shape(

 vgg_19/conv4/conv4_1/biases/readIdentityvgg_19/conv4/conv4_1/biases*
T0*.
_class$
" loc:@vgg_19/conv4/conv4_1/biases
c
&vgg_19/conv4/conv4_1/convolution/ShapeConst*%
valueB"            *
dtype0
c
.vgg_19/conv4/conv4_1/convolution/dilation_rateConst*
valueB"      *
dtype0
Γ
 vgg_19/conv4/conv4_1/convolutionConv2Dvgg_19/pool3/MaxPool!vgg_19/conv4/conv4_1/weights/read*
paddingSAME*
strides
*
data_formatNHWC*
T0*
use_cudnn_on_gpu(

vgg_19/conv4/conv4_1/BiasAddBiasAdd vgg_19/conv4/conv4_1/convolution vgg_19/conv4/conv4_1/biases/read*
data_formatNHWC*
T0
H
vgg_19/conv4/conv4_1/ReluReluvgg_19/conv4/conv4_1/BiasAdd*
T0
«
=vgg_19/conv4/conv4_2/weights/Initializer/random_uniform/shapeConst*
dtype0*%
valueB"            */
_class%
#!loc:@vgg_19/conv4/conv4_2/weights

;vgg_19/conv4/conv4_2/weights/Initializer/random_uniform/minConst*
valueB
 *μΡΌ*/
_class%
#!loc:@vgg_19/conv4/conv4_2/weights*
dtype0

;vgg_19/conv4/conv4_2/weights/Initializer/random_uniform/maxConst*
valueB
 *μΡ<*/
_class%
#!loc:@vgg_19/conv4/conv4_2/weights*
dtype0
υ
Evgg_19/conv4/conv4_2/weights/Initializer/random_uniform/RandomUniformRandomUniform=vgg_19/conv4/conv4_2/weights/Initializer/random_uniform/shape*

seed *
T0*/
_class%
#!loc:@vgg_19/conv4/conv4_2/weights*
dtype0*
seed2 
φ
;vgg_19/conv4/conv4_2/weights/Initializer/random_uniform/subSub;vgg_19/conv4/conv4_2/weights/Initializer/random_uniform/max;vgg_19/conv4/conv4_2/weights/Initializer/random_uniform/min*/
_class%
#!loc:@vgg_19/conv4/conv4_2/weights*
T0

;vgg_19/conv4/conv4_2/weights/Initializer/random_uniform/mulMulEvgg_19/conv4/conv4_2/weights/Initializer/random_uniform/RandomUniform;vgg_19/conv4/conv4_2/weights/Initializer/random_uniform/sub*
T0*/
_class%
#!loc:@vgg_19/conv4/conv4_2/weights
ς
7vgg_19/conv4/conv4_2/weights/Initializer/random_uniformAdd;vgg_19/conv4/conv4_2/weights/Initializer/random_uniform/mul;vgg_19/conv4/conv4_2/weights/Initializer/random_uniform/min*/
_class%
#!loc:@vgg_19/conv4/conv4_2/weights*
T0
«
vgg_19/conv4/conv4_2/weights
VariableV2*
shared_name *
dtype0*
shape:*
	container */
_class%
#!loc:@vgg_19/conv4/conv4_2/weights
η
#vgg_19/conv4/conv4_2/weights/AssignAssignvgg_19/conv4/conv4_2/weights7vgg_19/conv4/conv4_2/weights/Initializer/random_uniform*
validate_shape(*/
_class%
#!loc:@vgg_19/conv4/conv4_2/weights*
T0*
use_locking(

!vgg_19/conv4/conv4_2/weights/readIdentityvgg_19/conv4/conv4_2/weights*
T0*/
_class%
#!loc:@vgg_19/conv4/conv4_2/weights

-vgg_19/conv4/conv4_2/biases/Initializer/zerosConst*
valueB*    *.
_class$
" loc:@vgg_19/conv4/conv4_2/biases*
dtype0

vgg_19/conv4/conv4_2/biases
VariableV2*
shape:*
shared_name *.
_class$
" loc:@vgg_19/conv4/conv4_2/biases*
dtype0*
	container 
Ϊ
"vgg_19/conv4/conv4_2/biases/AssignAssignvgg_19/conv4/conv4_2/biases-vgg_19/conv4/conv4_2/biases/Initializer/zeros*
validate_shape(*.
_class$
" loc:@vgg_19/conv4/conv4_2/biases*
T0*
use_locking(

 vgg_19/conv4/conv4_2/biases/readIdentityvgg_19/conv4/conv4_2/biases*
T0*.
_class$
" loc:@vgg_19/conv4/conv4_2/biases
c
&vgg_19/conv4/conv4_2/convolution/ShapeConst*
dtype0*%
valueB"            
c
.vgg_19/conv4/conv4_2/convolution/dilation_rateConst*
valueB"      *
dtype0
Θ
 vgg_19/conv4/conv4_2/convolutionConv2Dvgg_19/conv4/conv4_1/Relu!vgg_19/conv4/conv4_2/weights/read*
use_cudnn_on_gpu(*
T0*
strides
*
data_formatNHWC*
paddingSAME

vgg_19/conv4/conv4_2/BiasAddBiasAdd vgg_19/conv4/conv4_2/convolution vgg_19/conv4/conv4_2/biases/read*
data_formatNHWC*
T0
H
vgg_19/conv4/conv4_2/ReluReluvgg_19/conv4/conv4_2/BiasAdd*
T0
«
=vgg_19/conv4/conv4_3/weights/Initializer/random_uniform/shapeConst*
dtype0*%
valueB"            */
_class%
#!loc:@vgg_19/conv4/conv4_3/weights

;vgg_19/conv4/conv4_3/weights/Initializer/random_uniform/minConst*
valueB
 *μΡΌ*/
_class%
#!loc:@vgg_19/conv4/conv4_3/weights*
dtype0

;vgg_19/conv4/conv4_3/weights/Initializer/random_uniform/maxConst*
dtype0*
valueB
 *μΡ<*/
_class%
#!loc:@vgg_19/conv4/conv4_3/weights
υ
Evgg_19/conv4/conv4_3/weights/Initializer/random_uniform/RandomUniformRandomUniform=vgg_19/conv4/conv4_3/weights/Initializer/random_uniform/shape*
seed2 *
dtype0*/
_class%
#!loc:@vgg_19/conv4/conv4_3/weights*
T0*

seed 
φ
;vgg_19/conv4/conv4_3/weights/Initializer/random_uniform/subSub;vgg_19/conv4/conv4_3/weights/Initializer/random_uniform/max;vgg_19/conv4/conv4_3/weights/Initializer/random_uniform/min*/
_class%
#!loc:@vgg_19/conv4/conv4_3/weights*
T0

;vgg_19/conv4/conv4_3/weights/Initializer/random_uniform/mulMulEvgg_19/conv4/conv4_3/weights/Initializer/random_uniform/RandomUniform;vgg_19/conv4/conv4_3/weights/Initializer/random_uniform/sub*/
_class%
#!loc:@vgg_19/conv4/conv4_3/weights*
T0
ς
7vgg_19/conv4/conv4_3/weights/Initializer/random_uniformAdd;vgg_19/conv4/conv4_3/weights/Initializer/random_uniform/mul;vgg_19/conv4/conv4_3/weights/Initializer/random_uniform/min*/
_class%
#!loc:@vgg_19/conv4/conv4_3/weights*
T0
«
vgg_19/conv4/conv4_3/weights
VariableV2*
	container *
dtype0*/
_class%
#!loc:@vgg_19/conv4/conv4_3/weights*
shared_name *
shape:
η
#vgg_19/conv4/conv4_3/weights/AssignAssignvgg_19/conv4/conv4_3/weights7vgg_19/conv4/conv4_3/weights/Initializer/random_uniform*
validate_shape(*/
_class%
#!loc:@vgg_19/conv4/conv4_3/weights*
T0*
use_locking(

!vgg_19/conv4/conv4_3/weights/readIdentityvgg_19/conv4/conv4_3/weights*
T0*/
_class%
#!loc:@vgg_19/conv4/conv4_3/weights

-vgg_19/conv4/conv4_3/biases/Initializer/zerosConst*
dtype0*
valueB*    *.
_class$
" loc:@vgg_19/conv4/conv4_3/biases

vgg_19/conv4/conv4_3/biases
VariableV2*
	container *
dtype0*.
_class$
" loc:@vgg_19/conv4/conv4_3/biases*
shared_name *
shape:
Ϊ
"vgg_19/conv4/conv4_3/biases/AssignAssignvgg_19/conv4/conv4_3/biases-vgg_19/conv4/conv4_3/biases/Initializer/zeros*
use_locking(*
validate_shape(*
T0*.
_class$
" loc:@vgg_19/conv4/conv4_3/biases

 vgg_19/conv4/conv4_3/biases/readIdentityvgg_19/conv4/conv4_3/biases*.
_class$
" loc:@vgg_19/conv4/conv4_3/biases*
T0
c
&vgg_19/conv4/conv4_3/convolution/ShapeConst*%
valueB"            *
dtype0
c
.vgg_19/conv4/conv4_3/convolution/dilation_rateConst*
dtype0*
valueB"      
Θ
 vgg_19/conv4/conv4_3/convolutionConv2Dvgg_19/conv4/conv4_2/Relu!vgg_19/conv4/conv4_3/weights/read*
paddingSAME*
strides
*
data_formatNHWC*
T0*
use_cudnn_on_gpu(

vgg_19/conv4/conv4_3/BiasAddBiasAdd vgg_19/conv4/conv4_3/convolution vgg_19/conv4/conv4_3/biases/read*
T0*
data_formatNHWC
H
vgg_19/conv4/conv4_3/ReluReluvgg_19/conv4/conv4_3/BiasAdd*
T0
«
=vgg_19/conv4/conv4_4/weights/Initializer/random_uniform/shapeConst*
dtype0*%
valueB"            */
_class%
#!loc:@vgg_19/conv4/conv4_4/weights

;vgg_19/conv4/conv4_4/weights/Initializer/random_uniform/minConst*
dtype0*
valueB
 *μΡΌ*/
_class%
#!loc:@vgg_19/conv4/conv4_4/weights

;vgg_19/conv4/conv4_4/weights/Initializer/random_uniform/maxConst*
dtype0*
valueB
 *μΡ<*/
_class%
#!loc:@vgg_19/conv4/conv4_4/weights
υ
Evgg_19/conv4/conv4_4/weights/Initializer/random_uniform/RandomUniformRandomUniform=vgg_19/conv4/conv4_4/weights/Initializer/random_uniform/shape*/
_class%
#!loc:@vgg_19/conv4/conv4_4/weights*
seed2 *
T0*

seed *
dtype0
φ
;vgg_19/conv4/conv4_4/weights/Initializer/random_uniform/subSub;vgg_19/conv4/conv4_4/weights/Initializer/random_uniform/max;vgg_19/conv4/conv4_4/weights/Initializer/random_uniform/min*
T0*/
_class%
#!loc:@vgg_19/conv4/conv4_4/weights

;vgg_19/conv4/conv4_4/weights/Initializer/random_uniform/mulMulEvgg_19/conv4/conv4_4/weights/Initializer/random_uniform/RandomUniform;vgg_19/conv4/conv4_4/weights/Initializer/random_uniform/sub*/
_class%
#!loc:@vgg_19/conv4/conv4_4/weights*
T0
ς
7vgg_19/conv4/conv4_4/weights/Initializer/random_uniformAdd;vgg_19/conv4/conv4_4/weights/Initializer/random_uniform/mul;vgg_19/conv4/conv4_4/weights/Initializer/random_uniform/min*/
_class%
#!loc:@vgg_19/conv4/conv4_4/weights*
T0
«
vgg_19/conv4/conv4_4/weights
VariableV2*
shared_name *
dtype0*
shape:*
	container */
_class%
#!loc:@vgg_19/conv4/conv4_4/weights
η
#vgg_19/conv4/conv4_4/weights/AssignAssignvgg_19/conv4/conv4_4/weights7vgg_19/conv4/conv4_4/weights/Initializer/random_uniform*
validate_shape(*/
_class%
#!loc:@vgg_19/conv4/conv4_4/weights*
T0*
use_locking(

!vgg_19/conv4/conv4_4/weights/readIdentityvgg_19/conv4/conv4_4/weights*
T0*/
_class%
#!loc:@vgg_19/conv4/conv4_4/weights

-vgg_19/conv4/conv4_4/biases/Initializer/zerosConst*
dtype0*
valueB*    *.
_class$
" loc:@vgg_19/conv4/conv4_4/biases

vgg_19/conv4/conv4_4/biases
VariableV2*.
_class$
" loc:@vgg_19/conv4/conv4_4/biases*
	container *
shape:*
dtype0*
shared_name 
Ϊ
"vgg_19/conv4/conv4_4/biases/AssignAssignvgg_19/conv4/conv4_4/biases-vgg_19/conv4/conv4_4/biases/Initializer/zeros*
use_locking(*
T0*.
_class$
" loc:@vgg_19/conv4/conv4_4/biases*
validate_shape(

 vgg_19/conv4/conv4_4/biases/readIdentityvgg_19/conv4/conv4_4/biases*
T0*.
_class$
" loc:@vgg_19/conv4/conv4_4/biases
c
&vgg_19/conv4/conv4_4/convolution/ShapeConst*
dtype0*%
valueB"            
c
.vgg_19/conv4/conv4_4/convolution/dilation_rateConst*
dtype0*
valueB"      
Θ
 vgg_19/conv4/conv4_4/convolutionConv2Dvgg_19/conv4/conv4_3/Relu!vgg_19/conv4/conv4_4/weights/read*
paddingSAME*
strides
*
data_formatNHWC*
T0*
use_cudnn_on_gpu(

vgg_19/conv4/conv4_4/BiasAddBiasAdd vgg_19/conv4/conv4_4/convolution vgg_19/conv4/conv4_4/biases/read*
T0*
data_formatNHWC
H
vgg_19/conv4/conv4_4/ReluReluvgg_19/conv4/conv4_4/BiasAdd*
T0

vgg_19/pool4/MaxPoolMaxPoolvgg_19/conv4/conv4_4/Relu*
ksize
*
paddingVALID*
T0*
strides
*
data_formatNHWC
«
=vgg_19/conv5/conv5_1/weights/Initializer/random_uniform/shapeConst*
dtype0*%
valueB"            */
_class%
#!loc:@vgg_19/conv5/conv5_1/weights

;vgg_19/conv5/conv5_1/weights/Initializer/random_uniform/minConst*
valueB
 *μΡΌ*/
_class%
#!loc:@vgg_19/conv5/conv5_1/weights*
dtype0

;vgg_19/conv5/conv5_1/weights/Initializer/random_uniform/maxConst*
dtype0*
valueB
 *μΡ<*/
_class%
#!loc:@vgg_19/conv5/conv5_1/weights
υ
Evgg_19/conv5/conv5_1/weights/Initializer/random_uniform/RandomUniformRandomUniform=vgg_19/conv5/conv5_1/weights/Initializer/random_uniform/shape*
seed2 *
dtype0*/
_class%
#!loc:@vgg_19/conv5/conv5_1/weights*
T0*

seed 
φ
;vgg_19/conv5/conv5_1/weights/Initializer/random_uniform/subSub;vgg_19/conv5/conv5_1/weights/Initializer/random_uniform/max;vgg_19/conv5/conv5_1/weights/Initializer/random_uniform/min*
T0*/
_class%
#!loc:@vgg_19/conv5/conv5_1/weights

;vgg_19/conv5/conv5_1/weights/Initializer/random_uniform/mulMulEvgg_19/conv5/conv5_1/weights/Initializer/random_uniform/RandomUniform;vgg_19/conv5/conv5_1/weights/Initializer/random_uniform/sub*/
_class%
#!loc:@vgg_19/conv5/conv5_1/weights*
T0
ς
7vgg_19/conv5/conv5_1/weights/Initializer/random_uniformAdd;vgg_19/conv5/conv5_1/weights/Initializer/random_uniform/mul;vgg_19/conv5/conv5_1/weights/Initializer/random_uniform/min*/
_class%
#!loc:@vgg_19/conv5/conv5_1/weights*
T0
«
vgg_19/conv5/conv5_1/weights
VariableV2*
shape:*
shared_name */
_class%
#!loc:@vgg_19/conv5/conv5_1/weights*
dtype0*
	container 
η
#vgg_19/conv5/conv5_1/weights/AssignAssignvgg_19/conv5/conv5_1/weights7vgg_19/conv5/conv5_1/weights/Initializer/random_uniform*
validate_shape(*/
_class%
#!loc:@vgg_19/conv5/conv5_1/weights*
T0*
use_locking(

!vgg_19/conv5/conv5_1/weights/readIdentityvgg_19/conv5/conv5_1/weights*/
_class%
#!loc:@vgg_19/conv5/conv5_1/weights*
T0

-vgg_19/conv5/conv5_1/biases/Initializer/zerosConst*
dtype0*
valueB*    *.
_class$
" loc:@vgg_19/conv5/conv5_1/biases

vgg_19/conv5/conv5_1/biases
VariableV2*
	container *
dtype0*.
_class$
" loc:@vgg_19/conv5/conv5_1/biases*
shared_name *
shape:
Ϊ
"vgg_19/conv5/conv5_1/biases/AssignAssignvgg_19/conv5/conv5_1/biases-vgg_19/conv5/conv5_1/biases/Initializer/zeros*
use_locking(*
T0*.
_class$
" loc:@vgg_19/conv5/conv5_1/biases*
validate_shape(

 vgg_19/conv5/conv5_1/biases/readIdentityvgg_19/conv5/conv5_1/biases*.
_class$
" loc:@vgg_19/conv5/conv5_1/biases*
T0
c
&vgg_19/conv5/conv5_1/convolution/ShapeConst*
dtype0*%
valueB"            
c
.vgg_19/conv5/conv5_1/convolution/dilation_rateConst*
dtype0*
valueB"      
Γ
 vgg_19/conv5/conv5_1/convolutionConv2Dvgg_19/pool4/MaxPool!vgg_19/conv5/conv5_1/weights/read*
paddingSAME*
strides
*
data_formatNHWC*
T0*
use_cudnn_on_gpu(

vgg_19/conv5/conv5_1/BiasAddBiasAdd vgg_19/conv5/conv5_1/convolution vgg_19/conv5/conv5_1/biases/read*
data_formatNHWC*
T0
H
vgg_19/conv5/conv5_1/ReluReluvgg_19/conv5/conv5_1/BiasAdd*
T0
«
=vgg_19/conv5/conv5_2/weights/Initializer/random_uniform/shapeConst*%
valueB"            */
_class%
#!loc:@vgg_19/conv5/conv5_2/weights*
dtype0

;vgg_19/conv5/conv5_2/weights/Initializer/random_uniform/minConst*
dtype0*
valueB
 *μΡΌ*/
_class%
#!loc:@vgg_19/conv5/conv5_2/weights

;vgg_19/conv5/conv5_2/weights/Initializer/random_uniform/maxConst*
valueB
 *μΡ<*/
_class%
#!loc:@vgg_19/conv5/conv5_2/weights*
dtype0
υ
Evgg_19/conv5/conv5_2/weights/Initializer/random_uniform/RandomUniformRandomUniform=vgg_19/conv5/conv5_2/weights/Initializer/random_uniform/shape*/
_class%
#!loc:@vgg_19/conv5/conv5_2/weights*
seed2 *
T0*

seed *
dtype0
φ
;vgg_19/conv5/conv5_2/weights/Initializer/random_uniform/subSub;vgg_19/conv5/conv5_2/weights/Initializer/random_uniform/max;vgg_19/conv5/conv5_2/weights/Initializer/random_uniform/min*
T0*/
_class%
#!loc:@vgg_19/conv5/conv5_2/weights

;vgg_19/conv5/conv5_2/weights/Initializer/random_uniform/mulMulEvgg_19/conv5/conv5_2/weights/Initializer/random_uniform/RandomUniform;vgg_19/conv5/conv5_2/weights/Initializer/random_uniform/sub*/
_class%
#!loc:@vgg_19/conv5/conv5_2/weights*
T0
ς
7vgg_19/conv5/conv5_2/weights/Initializer/random_uniformAdd;vgg_19/conv5/conv5_2/weights/Initializer/random_uniform/mul;vgg_19/conv5/conv5_2/weights/Initializer/random_uniform/min*
T0*/
_class%
#!loc:@vgg_19/conv5/conv5_2/weights
«
vgg_19/conv5/conv5_2/weights
VariableV2*
	container *
dtype0*/
_class%
#!loc:@vgg_19/conv5/conv5_2/weights*
shared_name *
shape:
η
#vgg_19/conv5/conv5_2/weights/AssignAssignvgg_19/conv5/conv5_2/weights7vgg_19/conv5/conv5_2/weights/Initializer/random_uniform*/
_class%
#!loc:@vgg_19/conv5/conv5_2/weights*
T0*
validate_shape(*
use_locking(

!vgg_19/conv5/conv5_2/weights/readIdentityvgg_19/conv5/conv5_2/weights*/
_class%
#!loc:@vgg_19/conv5/conv5_2/weights*
T0

-vgg_19/conv5/conv5_2/biases/Initializer/zerosConst*
valueB*    *.
_class$
" loc:@vgg_19/conv5/conv5_2/biases*
dtype0

vgg_19/conv5/conv5_2/biases
VariableV2*.
_class$
" loc:@vgg_19/conv5/conv5_2/biases*
	container *
shape:*
dtype0*
shared_name 
Ϊ
"vgg_19/conv5/conv5_2/biases/AssignAssignvgg_19/conv5/conv5_2/biases-vgg_19/conv5/conv5_2/biases/Initializer/zeros*
use_locking(*
T0*.
_class$
" loc:@vgg_19/conv5/conv5_2/biases*
validate_shape(

 vgg_19/conv5/conv5_2/biases/readIdentityvgg_19/conv5/conv5_2/biases*.
_class$
" loc:@vgg_19/conv5/conv5_2/biases*
T0
c
&vgg_19/conv5/conv5_2/convolution/ShapeConst*
dtype0*%
valueB"            
c
.vgg_19/conv5/conv5_2/convolution/dilation_rateConst*
valueB"      *
dtype0
Θ
 vgg_19/conv5/conv5_2/convolutionConv2Dvgg_19/conv5/conv5_1/Relu!vgg_19/conv5/conv5_2/weights/read*
paddingSAME*
strides
*
data_formatNHWC*
T0*
use_cudnn_on_gpu(

vgg_19/conv5/conv5_2/BiasAddBiasAdd vgg_19/conv5/conv5_2/convolution vgg_19/conv5/conv5_2/biases/read*
data_formatNHWC*
T0
H
vgg_19/conv5/conv5_2/ReluReluvgg_19/conv5/conv5_2/BiasAdd*
T0
«
=vgg_19/conv5/conv5_3/weights/Initializer/random_uniform/shapeConst*%
valueB"            */
_class%
#!loc:@vgg_19/conv5/conv5_3/weights*
dtype0

;vgg_19/conv5/conv5_3/weights/Initializer/random_uniform/minConst*
valueB
 *μΡΌ*/
_class%
#!loc:@vgg_19/conv5/conv5_3/weights*
dtype0

;vgg_19/conv5/conv5_3/weights/Initializer/random_uniform/maxConst*
dtype0*
valueB
 *μΡ<*/
_class%
#!loc:@vgg_19/conv5/conv5_3/weights
υ
Evgg_19/conv5/conv5_3/weights/Initializer/random_uniform/RandomUniformRandomUniform=vgg_19/conv5/conv5_3/weights/Initializer/random_uniform/shape*
seed2 *
dtype0*/
_class%
#!loc:@vgg_19/conv5/conv5_3/weights*
T0*

seed 
φ
;vgg_19/conv5/conv5_3/weights/Initializer/random_uniform/subSub;vgg_19/conv5/conv5_3/weights/Initializer/random_uniform/max;vgg_19/conv5/conv5_3/weights/Initializer/random_uniform/min*/
_class%
#!loc:@vgg_19/conv5/conv5_3/weights*
T0

;vgg_19/conv5/conv5_3/weights/Initializer/random_uniform/mulMulEvgg_19/conv5/conv5_3/weights/Initializer/random_uniform/RandomUniform;vgg_19/conv5/conv5_3/weights/Initializer/random_uniform/sub*/
_class%
#!loc:@vgg_19/conv5/conv5_3/weights*
T0
ς
7vgg_19/conv5/conv5_3/weights/Initializer/random_uniformAdd;vgg_19/conv5/conv5_3/weights/Initializer/random_uniform/mul;vgg_19/conv5/conv5_3/weights/Initializer/random_uniform/min*
T0*/
_class%
#!loc:@vgg_19/conv5/conv5_3/weights
«
vgg_19/conv5/conv5_3/weights
VariableV2*
shared_name *
dtype0*
shape:*
	container */
_class%
#!loc:@vgg_19/conv5/conv5_3/weights
η
#vgg_19/conv5/conv5_3/weights/AssignAssignvgg_19/conv5/conv5_3/weights7vgg_19/conv5/conv5_3/weights/Initializer/random_uniform*
validate_shape(*/
_class%
#!loc:@vgg_19/conv5/conv5_3/weights*
T0*
use_locking(

!vgg_19/conv5/conv5_3/weights/readIdentityvgg_19/conv5/conv5_3/weights*
T0*/
_class%
#!loc:@vgg_19/conv5/conv5_3/weights

-vgg_19/conv5/conv5_3/biases/Initializer/zerosConst*
valueB*    *.
_class$
" loc:@vgg_19/conv5/conv5_3/biases*
dtype0

vgg_19/conv5/conv5_3/biases
VariableV2*
	container *
dtype0*.
_class$
" loc:@vgg_19/conv5/conv5_3/biases*
shared_name *
shape:
Ϊ
"vgg_19/conv5/conv5_3/biases/AssignAssignvgg_19/conv5/conv5_3/biases-vgg_19/conv5/conv5_3/biases/Initializer/zeros*
use_locking(*
T0*.
_class$
" loc:@vgg_19/conv5/conv5_3/biases*
validate_shape(

 vgg_19/conv5/conv5_3/biases/readIdentityvgg_19/conv5/conv5_3/biases*
T0*.
_class$
" loc:@vgg_19/conv5/conv5_3/biases
c
&vgg_19/conv5/conv5_3/convolution/ShapeConst*
dtype0*%
valueB"            
c
.vgg_19/conv5/conv5_3/convolution/dilation_rateConst*
valueB"      *
dtype0
Θ
 vgg_19/conv5/conv5_3/convolutionConv2Dvgg_19/conv5/conv5_2/Relu!vgg_19/conv5/conv5_3/weights/read*
paddingSAME*
strides
*
data_formatNHWC*
T0*
use_cudnn_on_gpu(

vgg_19/conv5/conv5_3/BiasAddBiasAdd vgg_19/conv5/conv5_3/convolution vgg_19/conv5/conv5_3/biases/read*
data_formatNHWC*
T0
H
vgg_19/conv5/conv5_3/ReluReluvgg_19/conv5/conv5_3/BiasAdd*
T0
«
=vgg_19/conv5/conv5_4/weights/Initializer/random_uniform/shapeConst*
dtype0*%
valueB"            */
_class%
#!loc:@vgg_19/conv5/conv5_4/weights

;vgg_19/conv5/conv5_4/weights/Initializer/random_uniform/minConst*
dtype0*
valueB
 *μΡΌ*/
_class%
#!loc:@vgg_19/conv5/conv5_4/weights

;vgg_19/conv5/conv5_4/weights/Initializer/random_uniform/maxConst*
dtype0*
valueB
 *μΡ<*/
_class%
#!loc:@vgg_19/conv5/conv5_4/weights
υ
Evgg_19/conv5/conv5_4/weights/Initializer/random_uniform/RandomUniformRandomUniform=vgg_19/conv5/conv5_4/weights/Initializer/random_uniform/shape*

seed *
T0*/
_class%
#!loc:@vgg_19/conv5/conv5_4/weights*
dtype0*
seed2 
φ
;vgg_19/conv5/conv5_4/weights/Initializer/random_uniform/subSub;vgg_19/conv5/conv5_4/weights/Initializer/random_uniform/max;vgg_19/conv5/conv5_4/weights/Initializer/random_uniform/min*
T0*/
_class%
#!loc:@vgg_19/conv5/conv5_4/weights

;vgg_19/conv5/conv5_4/weights/Initializer/random_uniform/mulMulEvgg_19/conv5/conv5_4/weights/Initializer/random_uniform/RandomUniform;vgg_19/conv5/conv5_4/weights/Initializer/random_uniform/sub*/
_class%
#!loc:@vgg_19/conv5/conv5_4/weights*
T0
ς
7vgg_19/conv5/conv5_4/weights/Initializer/random_uniformAdd;vgg_19/conv5/conv5_4/weights/Initializer/random_uniform/mul;vgg_19/conv5/conv5_4/weights/Initializer/random_uniform/min*/
_class%
#!loc:@vgg_19/conv5/conv5_4/weights*
T0
«
vgg_19/conv5/conv5_4/weights
VariableV2*
shared_name *
dtype0*
shape:*
	container */
_class%
#!loc:@vgg_19/conv5/conv5_4/weights
η
#vgg_19/conv5/conv5_4/weights/AssignAssignvgg_19/conv5/conv5_4/weights7vgg_19/conv5/conv5_4/weights/Initializer/random_uniform*
validate_shape(*/
_class%
#!loc:@vgg_19/conv5/conv5_4/weights*
T0*
use_locking(

!vgg_19/conv5/conv5_4/weights/readIdentityvgg_19/conv5/conv5_4/weights*
T0*/
_class%
#!loc:@vgg_19/conv5/conv5_4/weights

-vgg_19/conv5/conv5_4/biases/Initializer/zerosConst*
dtype0*
valueB*    *.
_class$
" loc:@vgg_19/conv5/conv5_4/biases

vgg_19/conv5/conv5_4/biases
VariableV2*.
_class$
" loc:@vgg_19/conv5/conv5_4/biases*
	container *
shape:*
dtype0*
shared_name 
Ϊ
"vgg_19/conv5/conv5_4/biases/AssignAssignvgg_19/conv5/conv5_4/biases-vgg_19/conv5/conv5_4/biases/Initializer/zeros*
use_locking(*
validate_shape(*
T0*.
_class$
" loc:@vgg_19/conv5/conv5_4/biases

 vgg_19/conv5/conv5_4/biases/readIdentityvgg_19/conv5/conv5_4/biases*
T0*.
_class$
" loc:@vgg_19/conv5/conv5_4/biases
c
&vgg_19/conv5/conv5_4/convolution/ShapeConst*
dtype0*%
valueB"            
c
.vgg_19/conv5/conv5_4/convolution/dilation_rateConst*
dtype0*
valueB"      
Θ
 vgg_19/conv5/conv5_4/convolutionConv2Dvgg_19/conv5/conv5_3/Relu!vgg_19/conv5/conv5_4/weights/read*
use_cudnn_on_gpu(*
T0*
strides
*
data_formatNHWC*
paddingSAME

vgg_19/conv5/conv5_4/BiasAddBiasAdd vgg_19/conv5/conv5_4/convolution vgg_19/conv5/conv5_4/biases/read*
T0*
data_formatNHWC
H
vgg_19/conv5/conv5_4/ReluReluvgg_19/conv5/conv5_4/BiasAdd*
T0

vgg_19/pool5/MaxPoolMaxPoolvgg_19/conv5/conv5_4/Relu*
ksize
*
T0*
strides
*
data_formatNHWC*
paddingVALID

3vgg_19/fc6/weights/Initializer/random_uniform/shapeConst*
dtype0*%
valueB"            *%
_class
loc:@vgg_19/fc6/weights

1vgg_19/fc6/weights/Initializer/random_uniform/minConst*
dtype0*
valueB
 *κ¨»*%
_class
loc:@vgg_19/fc6/weights

1vgg_19/fc6/weights/Initializer/random_uniform/maxConst*
valueB
 *κ¨;*%
_class
loc:@vgg_19/fc6/weights*
dtype0
Χ
;vgg_19/fc6/weights/Initializer/random_uniform/RandomUniformRandomUniform3vgg_19/fc6/weights/Initializer/random_uniform/shape*

seed *
T0*%
_class
loc:@vgg_19/fc6/weights*
dtype0*
seed2 
Ξ
1vgg_19/fc6/weights/Initializer/random_uniform/subSub1vgg_19/fc6/weights/Initializer/random_uniform/max1vgg_19/fc6/weights/Initializer/random_uniform/min*%
_class
loc:@vgg_19/fc6/weights*
T0
Ψ
1vgg_19/fc6/weights/Initializer/random_uniform/mulMul;vgg_19/fc6/weights/Initializer/random_uniform/RandomUniform1vgg_19/fc6/weights/Initializer/random_uniform/sub*
T0*%
_class
loc:@vgg_19/fc6/weights
Κ
-vgg_19/fc6/weights/Initializer/random_uniformAdd1vgg_19/fc6/weights/Initializer/random_uniform/mul1vgg_19/fc6/weights/Initializer/random_uniform/min*%
_class
loc:@vgg_19/fc6/weights*
T0

vgg_19/fc6/weights
VariableV2*
shape: *
shared_name *%
_class
loc:@vgg_19/fc6/weights*
dtype0*
	container 
Ώ
vgg_19/fc6/weights/AssignAssignvgg_19/fc6/weights-vgg_19/fc6/weights/Initializer/random_uniform*
use_locking(*
T0*%
_class
loc:@vgg_19/fc6/weights*
validate_shape(
g
vgg_19/fc6/weights/readIdentityvgg_19/fc6/weights*%
_class
loc:@vgg_19/fc6/weights*
T0
{
#vgg_19/fc6/biases/Initializer/zerosConst*
dtype0*
valueB *    *$
_class
loc:@vgg_19/fc6/biases

vgg_19/fc6/biases
VariableV2*
	container *
dtype0*$
_class
loc:@vgg_19/fc6/biases*
shared_name *
shape: 
²
vgg_19/fc6/biases/AssignAssignvgg_19/fc6/biases#vgg_19/fc6/biases/Initializer/zeros*
validate_shape(*$
_class
loc:@vgg_19/fc6/biases*
T0*
use_locking(
d
vgg_19/fc6/biases/readIdentityvgg_19/fc6/biases*
T0*$
_class
loc:@vgg_19/fc6/biases
Y
vgg_19/fc6/convolution/ShapeConst*%
valueB"            *
dtype0
Y
$vgg_19/fc6/convolution/dilation_rateConst*
dtype0*
valueB"      
°
vgg_19/fc6/convolutionConv2Dvgg_19/pool5/MaxPoolvgg_19/fc6/weights/read*
paddingVALID*
strides
*
data_formatNHWC*
T0*
use_cudnn_on_gpu(
m
vgg_19/fc6/BiasAddBiasAddvgg_19/fc6/convolutionvgg_19/fc6/biases/read*
T0*
data_formatNHWC
4
vgg_19/fc6/ReluReluvgg_19/fc6/BiasAdd*
T0
N
!vgg_19/dropout6/dropout/keep_probConst*
valueB
 *   ?*
dtype0
Z
vgg_19/dropout6/dropout/ShapeConst*
dtype0*%
valueB"            
W
*vgg_19/dropout6/dropout/random_uniform/minConst*
valueB
 *    *
dtype0
W
*vgg_19/dropout6/dropout/random_uniform/maxConst*
dtype0*
valueB
 *  ?

4vgg_19/dropout6/dropout/random_uniform/RandomUniformRandomUniformvgg_19/dropout6/dropout/Shape*
seed2 *
dtype0*
T0*

seed 

*vgg_19/dropout6/dropout/random_uniform/subSub*vgg_19/dropout6/dropout/random_uniform/max*vgg_19/dropout6/dropout/random_uniform/min*
T0

*vgg_19/dropout6/dropout/random_uniform/mulMul4vgg_19/dropout6/dropout/random_uniform/RandomUniform*vgg_19/dropout6/dropout/random_uniform/sub*
T0

&vgg_19/dropout6/dropout/random_uniformAdd*vgg_19/dropout6/dropout/random_uniform/mul*vgg_19/dropout6/dropout/random_uniform/min*
T0
v
vgg_19/dropout6/dropout/addAdd!vgg_19/dropout6/dropout/keep_prob&vgg_19/dropout6/dropout/random_uniform*
T0
L
vgg_19/dropout6/dropout/FloorFloorvgg_19/dropout6/dropout/add*
T0
c
vgg_19/dropout6/dropout/divRealDivvgg_19/fc6/Relu!vgg_19/dropout6/dropout/keep_prob*
T0
g
vgg_19/dropout6/dropout/mulMulvgg_19/dropout6/dropout/divvgg_19/dropout6/dropout/Floor*
T0

3vgg_19/fc7/weights/Initializer/random_uniform/shapeConst*
dtype0*%
valueB"            *%
_class
loc:@vgg_19/fc7/weights

1vgg_19/fc7/weights/Initializer/random_uniform/minConst*
valueB
 *Χ³έΌ*%
_class
loc:@vgg_19/fc7/weights*
dtype0

1vgg_19/fc7/weights/Initializer/random_uniform/maxConst*
valueB
 *Χ³έ<*%
_class
loc:@vgg_19/fc7/weights*
dtype0
Χ
;vgg_19/fc7/weights/Initializer/random_uniform/RandomUniformRandomUniform3vgg_19/fc7/weights/Initializer/random_uniform/shape*
dtype0*

seed *
T0*
seed2 *%
_class
loc:@vgg_19/fc7/weights
Ξ
1vgg_19/fc7/weights/Initializer/random_uniform/subSub1vgg_19/fc7/weights/Initializer/random_uniform/max1vgg_19/fc7/weights/Initializer/random_uniform/min*%
_class
loc:@vgg_19/fc7/weights*
T0
Ψ
1vgg_19/fc7/weights/Initializer/random_uniform/mulMul;vgg_19/fc7/weights/Initializer/random_uniform/RandomUniform1vgg_19/fc7/weights/Initializer/random_uniform/sub*
T0*%
_class
loc:@vgg_19/fc7/weights
Κ
-vgg_19/fc7/weights/Initializer/random_uniformAdd1vgg_19/fc7/weights/Initializer/random_uniform/mul1vgg_19/fc7/weights/Initializer/random_uniform/min*%
_class
loc:@vgg_19/fc7/weights*
T0

vgg_19/fc7/weights
VariableV2*
shape:  *
shared_name *%
_class
loc:@vgg_19/fc7/weights*
dtype0*
	container 
Ώ
vgg_19/fc7/weights/AssignAssignvgg_19/fc7/weights-vgg_19/fc7/weights/Initializer/random_uniform*%
_class
loc:@vgg_19/fc7/weights*
T0*
validate_shape(*
use_locking(
g
vgg_19/fc7/weights/readIdentityvgg_19/fc7/weights*
T0*%
_class
loc:@vgg_19/fc7/weights
{
#vgg_19/fc7/biases/Initializer/zerosConst*
valueB *    *$
_class
loc:@vgg_19/fc7/biases*
dtype0

vgg_19/fc7/biases
VariableV2*
shared_name *
dtype0*
shape: *
	container *$
_class
loc:@vgg_19/fc7/biases
²
vgg_19/fc7/biases/AssignAssignvgg_19/fc7/biases#vgg_19/fc7/biases/Initializer/zeros*$
_class
loc:@vgg_19/fc7/biases*
T0*
validate_shape(*
use_locking(
d
vgg_19/fc7/biases/readIdentityvgg_19/fc7/biases*
T0*$
_class
loc:@vgg_19/fc7/biases
Y
vgg_19/fc7/convolution/ShapeConst*%
valueB"            *
dtype0
Y
$vgg_19/fc7/convolution/dilation_rateConst*
dtype0*
valueB"      
Ά
vgg_19/fc7/convolutionConv2Dvgg_19/dropout6/dropout/mulvgg_19/fc7/weights/read*
use_cudnn_on_gpu(*
T0*
strides
*
data_formatNHWC*
paddingSAME
m
vgg_19/fc7/BiasAddBiasAddvgg_19/fc7/convolutionvgg_19/fc7/biases/read*
data_formatNHWC*
T0
4
vgg_19/fc7/ReluReluvgg_19/fc7/BiasAdd*
T0
N
!vgg_19/dropout7/dropout/keep_probConst*
dtype0*
valueB
 *   ?
Z
vgg_19/dropout7/dropout/ShapeConst*%
valueB"            *
dtype0
W
*vgg_19/dropout7/dropout/random_uniform/minConst*
valueB
 *    *
dtype0
W
*vgg_19/dropout7/dropout/random_uniform/maxConst*
valueB
 *  ?*
dtype0

4vgg_19/dropout7/dropout/random_uniform/RandomUniformRandomUniformvgg_19/dropout7/dropout/Shape*
seed2 *
dtype0*
T0*

seed 

*vgg_19/dropout7/dropout/random_uniform/subSub*vgg_19/dropout7/dropout/random_uniform/max*vgg_19/dropout7/dropout/random_uniform/min*
T0

*vgg_19/dropout7/dropout/random_uniform/mulMul4vgg_19/dropout7/dropout/random_uniform/RandomUniform*vgg_19/dropout7/dropout/random_uniform/sub*
T0

&vgg_19/dropout7/dropout/random_uniformAdd*vgg_19/dropout7/dropout/random_uniform/mul*vgg_19/dropout7/dropout/random_uniform/min*
T0
v
vgg_19/dropout7/dropout/addAdd!vgg_19/dropout7/dropout/keep_prob&vgg_19/dropout7/dropout/random_uniform*
T0
L
vgg_19/dropout7/dropout/FloorFloorvgg_19/dropout7/dropout/add*
T0
c
vgg_19/dropout7/dropout/divRealDivvgg_19/fc7/Relu!vgg_19/dropout7/dropout/keep_prob*
T0
g
vgg_19/dropout7/dropout/mulMulvgg_19/dropout7/dropout/divvgg_19/dropout7/dropout/Floor*
T0

3vgg_19/fc8/weights/Initializer/random_uniform/shapeConst*%
valueB"         θ  *%
_class
loc:@vgg_19/fc8/weights*
dtype0

1vgg_19/fc8/weights/Initializer/random_uniform/minConst*
dtype0*
valueB
 *ω½*%
_class
loc:@vgg_19/fc8/weights

1vgg_19/fc8/weights/Initializer/random_uniform/maxConst*
dtype0*
valueB
 *ω=*%
_class
loc:@vgg_19/fc8/weights
Χ
;vgg_19/fc8/weights/Initializer/random_uniform/RandomUniformRandomUniform3vgg_19/fc8/weights/Initializer/random_uniform/shape*
dtype0*

seed *
T0*
seed2 *%
_class
loc:@vgg_19/fc8/weights
Ξ
1vgg_19/fc8/weights/Initializer/random_uniform/subSub1vgg_19/fc8/weights/Initializer/random_uniform/max1vgg_19/fc8/weights/Initializer/random_uniform/min*%
_class
loc:@vgg_19/fc8/weights*
T0
Ψ
1vgg_19/fc8/weights/Initializer/random_uniform/mulMul;vgg_19/fc8/weights/Initializer/random_uniform/RandomUniform1vgg_19/fc8/weights/Initializer/random_uniform/sub*
T0*%
_class
loc:@vgg_19/fc8/weights
Κ
-vgg_19/fc8/weights/Initializer/random_uniformAdd1vgg_19/fc8/weights/Initializer/random_uniform/mul1vgg_19/fc8/weights/Initializer/random_uniform/min*
T0*%
_class
loc:@vgg_19/fc8/weights

vgg_19/fc8/weights
VariableV2*%
_class
loc:@vgg_19/fc8/weights*
	container *
shape: θ*
dtype0*
shared_name 
Ώ
vgg_19/fc8/weights/AssignAssignvgg_19/fc8/weights-vgg_19/fc8/weights/Initializer/random_uniform*
use_locking(*
validate_shape(*
T0*%
_class
loc:@vgg_19/fc8/weights
g
vgg_19/fc8/weights/readIdentityvgg_19/fc8/weights*
T0*%
_class
loc:@vgg_19/fc8/weights
{
#vgg_19/fc8/biases/Initializer/zerosConst*
dtype0*
valueBθ*    *$
_class
loc:@vgg_19/fc8/biases

vgg_19/fc8/biases
VariableV2*
shape:θ*
shared_name *$
_class
loc:@vgg_19/fc8/biases*
dtype0*
	container 
²
vgg_19/fc8/biases/AssignAssignvgg_19/fc8/biases#vgg_19/fc8/biases/Initializer/zeros*$
_class
loc:@vgg_19/fc8/biases*
T0*
validate_shape(*
use_locking(
d
vgg_19/fc8/biases/readIdentityvgg_19/fc8/biases*$
_class
loc:@vgg_19/fc8/biases*
T0
Y
vgg_19/fc8/convolution/ShapeConst*%
valueB"         θ  *
dtype0
Y
$vgg_19/fc8/convolution/dilation_rateConst*
dtype0*
valueB"      
Ά
vgg_19/fc8/convolutionConv2Dvgg_19/dropout7/dropout/mulvgg_19/fc8/weights/read*
paddingSAME*
strides
*
data_formatNHWC*
T0*
use_cudnn_on_gpu(
m
vgg_19/fc8/BiasAddBiasAddvgg_19/fc8/convolutionvgg_19/fc8/biases/read*
data_formatNHWC*
T0
S
vgg_19/fc8/squeezedSqueezevgg_19/fc8/BiasAdd*
T0*
squeeze_dims
"
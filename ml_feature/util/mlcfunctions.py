#coding=utf8
# Copyright (c) 2016 Tinydot. inc.
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import json

def mlc_json_encoder(op_feature):
    return json.dumps(op_feature, ensure_ascii=False, indent=2)

def mlc_json_decoder(mlc_text):
    return json.loads(mlc_text)

def mlc_default_encoder(op_feature):
    return mlc_json_encoder(op_feature)

def mlc_default_decoder(mlc_text):
    return mlc_json_decoder(mlc_text)


def mlc_encoders():
    """
    伪指令流格式编码
    :return:
    """
    return {"default": mlc_default_encoder,
            "json": mlc_json_encoder,
            }

def mlc_decoders():
    """
    伪指令流格式解码
    :return:
    """
    return {"default": mlc_default_decoder,
            "json": mlc_json_decoder,
            }

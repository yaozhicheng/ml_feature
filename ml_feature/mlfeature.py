#!/usr/bin/env python
#coding=utf-8
# Copyright (c) 2016 Tinydot. inc.
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from os import path
import sys
sys.path.append(path.abspath(path.join(path.dirname(__file__), "..")))

import logging
logging.basicConfig(level=logging.ERROR)

from ml_feature.util.logger import setLevel, DEBUG, ERROR, LOG
setLevel(ERROR)

import argparse

def main(argv=None):
    """App Entry"""

    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--debug", default=False, action="store_true", help="enable debug output")

    args = parser.parse_args(argv)
    if args.debug is True:
        setLevel(DEBUG)

        LOG.debug("enable debug output")

    from ml_feature.core.mainview import start
    start()

if __name__ == "__main__":
    main(["-d"])

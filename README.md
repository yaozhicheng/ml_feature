### ml_feature

ml_feature 为神经网络特征提取工具

运行需求：

- python >= 3.4
- pip >= 9.0.1

### 运行：
##### 方式1: 直接运行
```
$ git clone https://git.oschina.net/yaozhicheng/ml_feature.git  # 下载
$ cd ml_feature
$ pip install -r requirements.txt  # 安装依赖
$ python ml_feature／mlfeature  # 运行
```

##### 方式2: 安装运行
```
$ git clone https://git.oschina.net/yaozhicheng/ml_feature.git  # 下载
$ cd ml_feature
$ python setpu.py install --user # 安装程序包到用户目录
$ mlfeature  # 直接命令行执行
```

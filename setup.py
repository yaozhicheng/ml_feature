#!/usr/bin/env python
#coding=utf-8
# Copyright (c) 2016 Tinydot. inc.
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

def load_readme():
    with open("README.rst") as f:
        return f.read()

def load_requirements():
    with open("requirements.txt") as f:
        return f.readlines()

REQUIREMENTS = load_requirements()

from setuptools import setup, find_packages

setup(
    install_requires = REQUIREMENTS,
    name = "ml_feature",
    author = "ict",
    author_email= "ml_feature@ict.ac.cn",
    version="0.0.1",

    packages = find_packages(),
    include_package_data = True,

    license=('Apache License 2.0)'),
    url="http://www.ml_feature.com",
    description="Machine Learning algorithm Feature extractor",

    long_description = "Machine Learning algorithm Feature extractor(ml_feature):"
                       "Extract tensorflow based ML model's characters"
                       "README:"
                       "%s"%load_readme(),

    platforms= "Win64, mac os x, linux",
    zip_safe = False,
    entry_points = {
        'console_scripts':[
            'mlfeature = ml_feature.mlfeature:main'
        ]
    }
)
